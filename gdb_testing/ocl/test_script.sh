#!/bin/bash
app=$1
gdb_script=$2
script_dir=`pwd`
continue_steps=1
#set this for debug
#set -x

if [ $# -ne 2 ]
then
     echo Useage: $0 example_name gdb_script_file
     exit 1
fi

if [ -z "$TI_OCL_EXAMPLES" ]
then
    TI_OCL_EXAMPLES="/usr/share/ti/examples/opencl"
fi 

if [ -z "$TI_MULTI_FFT" ]
then
    TI_MULTI_FFT="/usr/share/ti/examples/openmpi+opencl+openmp"
fi

if [ -z "$TI_OCL_OMP" ]
then
    TI_OCL_OMP="/usr/share/ti/examples/opencl+openmp"
fi

if [ "$app" == "multinode_fftdemo" ]
then
     pushd $TI_MULTI_FFT/$app/scripts
     app=run_multinode_fft.sh
     #the number of times the kernel must be continued
     #to finish execution
     continue_steps=25
elif echo "$app" | grep -q "openmp"
then
     pushd $TI_OCL_OMP/$app
else
     pushd $TI_OCL_EXAMPLES/$app
fi

# Create the fifos if not-existent
if [ ! -e read ]
then
    mkfifo read
fi

if [ ! -e write ]
then
    mkfifo write
fi

if [ -e output.txt ]
then
    rm output.txt
fi

# launch the opencl application
exec 3<>write
./$app <&3 > read &
cat read | \
while read line; do
	case "$line" in 
	*gdb*)
                #run the test, die after 30s
		echo $line
		eval "timeout 30s gdb${line#*gdb} --batch -silent --command=$script_dir/$gdb_script > /dev/null" &
		PID="$(ps u | grep timeout | awk 'FNR == 1 {print $2}')"
		echo gdb6x PID = $PID
		#wait for sanity tests to complete
		sleep 10
          for((i=0; i<$continue_steps; i++))
          do
               echo -e "c\n" >&3
               sleep 1
          done
		break
		;;
	esac
done
wait $PID


rm read
rm write

mv output.txt $script_dir/output_${app%.*}.txt
popd

#validate the results
./validate.sh output_${app%.*}.txt > test_summary_${app%.*}.txt











