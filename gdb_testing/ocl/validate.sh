#!/bin/bash
output=$1
#set -x
echo -e "GDB Test Summary for $output\n"

if [ ! -e $output ]
then
     echo -e File $output does not exist
     exit 1
fi

# check for failed connections
if grep -q "Connection refused." $output
then
    echo FAIL - Failed to connect to a DSP - connection refused
    exit 1
else
    echo PASS - No connections were refused
fi

if grep -q "Device or resource busy." $output
then
    echo FAIL - Failed to connect to a DSP - resource busy
    exit 1
else
    echo PASS - All connections were established
fi

# check connections to DSPs
for (( i=0; i<8; i++ ))
do
    if ! grep -xq "DSP $i:DNUM           0x$i	$i" $output
    then
	echo FAIL - Failed sanity check for DSP $i
	exit 1
    fi
done

echo "PASS - Connection sanity check passed"

if ! grep -q "Breakpoint 2," $output
then
    echo FAIL - Failed to halt at entry point
    exit
else
    echo PASS - Halted at entry point
fi

# should always halt at breakpoint 1 and 2
if ! grep -q "Breakpoint 1," $output
then
    echo FAIL - Failed to halt at exit \(expected\)
    #This fail is expected
    exit 0
else
    echo PASS - Halted at exit
fi

# ensure that test harness completed
if ! grep -q "END" $output
then
    echo FAIL - Failed to finish test
    exit 1
else
    echo PASS - Test Finished
fi












