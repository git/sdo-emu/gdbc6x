#!/bin/bash

#set the environment
if [ -z "$TI_OCL_DEBUG" ]
then
    source test_environment
fi 

#test the ocl examples
#they must be built with debug on
./test_script.sh edmamgr ocl_test.gdb
sleep 30
./test_script.sh matmpy ocl_test.gdb
sleep 30
./test_script.sh dsplib_fft ocl_test.gdb
sleep 30
./test_script.sh offline ocl_test.gdb
sleep 30
./test_script.sh vecadd_openmp ocl_test.gdb
sleep 30
./test_script.sh multinode_fftdemo ocl_test.gdb

