set pagination off                                                     
set logging file output.txt                                            
set logging on                                                         
                                                                       
# sanity testing                                                       
                                                                       
echo Sanity Testing the DSP connections...\n                              
tar rem /dev/gdbtty7                                                   
echo DSP 7:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty6                                                   
echo DSP 6:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty5                                                   
echo DSP 5:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty4                                                   
echo DSP 4:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty3                                                   
echo DSP 3:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty2                                                   
echo DSP 2:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty1                                                   
echo DSP 1:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty0                                                   
echo DSP 0:                                                            
info reg DNUM   

echo \nTesting breakpoints...\n
c

echo \nTesting single stepping...\n
si
info reg PC
si 
info reg PC
si
info reg PC

echo \nContinue to exit...\n
d 2
c

echo \nEND\n



