/*
 *  ======== main.c ========
 */

#include <xdc/std.h>

#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>

#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/family/c66/tci66xx/CpIntc.h>
#include <ti/sysbios/family/c66/Cache.h>
#include <stdbool.h>
#include "GDB_server.h"
#include <ti/sysbios/family/c64p/Hwi.h>

Hwi_Handle myHwi;
Void myIsr(UArg arg);

uint32_t sharedAddr    = 0;
bool	 initialized   = false;

int dostuff()
{
	int b = 0;
	b = 363;
	b++;
	b--;
}

int funcCall2(int *count)
{
	int a = 0;
	a++;
	dostuff();
}

int funcCall(int *count)
{
	int circle = *(uint32_t *) 0x0c000008;
	if(*count >= 0x5000000)
	{
		*(uint32_t *)0x0c000008 = circle + 1;
		*count = 0;
	}
	funcCall2(count);
}

Void myTask()
{
	int count = 0;
	*(int*)0x0c000008 = count;
	int index = 0;
	volatile int fake = 0;
	while(1)
	{
		count++;
		*(int*)0x0c000004 = count;
		funcCall(&count);
		//SPLOOP
		for(index = 0; index < 2000; index++)
		{
			*(int*)0x0c000008 = index;
		}

		if(fake)
		{
			// c66x specific instructions
			asm("	crot270 a0, a1");
			asm("	crot90 a0, a1");
			asm("	dadd.s1 a1:a0, a3:a2, a15:a14");
			asm("	dadd.l1 a1:a0, a3:a2, a15:a14");
			asm("	dadd2.s1 a1:a0, a3:a2, a15:a14");
			asm("	dadd2.l1 a1:a0, a3:a2, a15:a14");
			asm("	daddsp.s1 a1:a0, a3:a2, a15:a14");
			asm("	daddsp.l1 a1:a0, a3:a2, a15:a14");
			asm("	davg2 a1:a0, a3:a2, a15:a14");
			asm("	dapys2 a1:a0, a3:a2, a15:a14");
			asm("	davgnr2 a1:a0, a3:a2, a15:a14");
			asm("	davgnru4 a1:a0, a3:a2, a15:a14");
			asm("	davgu4 a1:a0, a3:a2, a15:a14");
			asm("	dccmpyr1 a1:a0, a3:a2, a15:a14");
			asm("	dcmpeq2 a1:a0, a3:a2, a15");
			asm("	dcmpeq4 a1:a0, a3:a2, a15");
			asm("	dcmpgt2 a1:a0, a3:a2, a15");
			asm("	dcmpgtu4 a1:a0, a3:a2, a15");
			asm("	dcmpyr1 a5:a4, a7:a6, a15:a14");
			asm("	dcrot270 a5:a4, a7:a6");
			asm("	dcrot90 a5:a4, a7:a6");
			asm("	dinthsp.l a2, a1:a0");
			asm("	dinthsp.s a2, a1:a0");
			asm("	dinthspu.1 a2, a1:a0");
			asm("	dinthspu.s a2, a1:a0");
			asm("	dmax2 a1:a0, a3:a2, a15:a14");
			asm("	dmaxu4 a1:a0, a3:a2, a15:a14");
			asm("	dmin2 a1:a0, a3:a2, a15:a14");
			asm("	dminu4 a1:a0, a3:a2, a15:a14");
			asm("	dmpysp a5:a4, a9:a8, a1:a0");
			asm("	dmvd.s a1, a6, a3:a2");
			asm("	dmvd.l a1, a6, a3:a2");
			asm("	dpackh2.s a1:a0, a9:a8, a1:a0");
			asm("	dpackh2.l a5:a4, a9:a8, a1:a0");
			asm("	dpackh4 a5:a4, a9:a8, a1:a0");
			asm("	dpackhl2.s a1:a0, a9:a8, a1:a0");
			asm("	dpackhl2.l a5:a4, a9:a8, a1:a0");
			asm("	dpackl2.s a1:a0, a9:a8, a1:a0");
			asm("	dpackl2.l a5:a4, a9:a8, a1:a0");
			asm("	dpackl4 a5:a4, a9:a8, a1:a0");
			asm("	dpacklh2.s a1:a0, a9:a8, a1:a0");
			asm("	dpacklh2.l a5:a4, a9:a8, a1:a0");
			asm("	dpacklh4 a5, a9, a1:a0");
			asm("	dsadd.s a1:a0, a9:a8, a1:a0");
			asm("	dsadd.l a5:a4, a5:a4, a1:a0");
			asm("	dsadd2.s a1:a0, a9:a8, a1:a0");
			asm("	dsadd2.l a5:a4, a9:a8, a1:a0");
			asm("	dshl a5:a4, a6, a1:a0");
			asm("	dshl a5:a4, 16, a1:a0");
			asm("	dshl2 a5:a4, a6, a1:a0");
			asm("	dshl2 a5:a4, 16, a1:a0");
			asm("	dshr a5:a4, a6, a1:a0");
			asm("	dshr a5:a4, 16, a1:a0");
			asm("	dshr2 a5:a4, a6, a1:a0");
			asm("	dshr2 a5:a4, 16, a1:a0");
			asm("	dshru a5:a4, a6, a1:a0");
			asm("	dshru a5:a4, 16, a1:a0");
			asm("	dshru2 a5:a4, a6, a1:a0");
			asm("	dshru2 a5:a4, 16, a1:a0");
			asm("	dspacku4 a5:a4, a5:a4, a1:a0");
			asm("	dspint.l a5:a4, a5:a4");
			asm("	dspint.s a5:a4, a5:a4");
			asm("	dspinth.l a5:a4, a5");
		    asm("	dspinth.s a5:a4, a5");
		    asm("	dssub a5:a4, a9:a8, a1:a0");
		    asm("	dssub2 a5:a4, a9:a8, a1:a0");
		    asm("	dsub.l a5:a4, a9:a8, a1:a0");
		    asm("	dsub.s a5:a4, a9:a8, a1:a0");
		    asm("	dsub2.l a5:a4, a9:a8, a1:a0");
		    asm("	dsub2.s a5:a4, a9:a8, a1:a0");
		    asm("	dxpnd2 a0, a3:a2");
		    asm("	dxpnd4 a0, a3:a2");
		    asm("	fadddp.l a5:a4, a9:a8, a1:a0");
		    asm("	fadddp.s a5:a4, a9:a8, a1:a0");
		    asm("	faddsp.l a4, a8, a0");
		    asm("	faddsp.s a4, a8, a0");
		    asm("	fmpydp a5:a4, a9:a8, a1:a0");
		    asm("	fsubdp.l a5:a4, a9:a8, a1:a0");
		    asm("	fsubdp.s a5:a4, a9:a8, a1:a0");
		    asm("	fsubsp.l a4, a8, a0");
		    asm("	fsubsp.s a4, a8, a0");
		    asm("	land a4, a8, a0");
		    asm("	landn a4, a8, a0");
		    asm("	lor a4, a8, a0");
		    asm("	mpyu2 a4, a8, a1:a0");
		    asm("	shl2 a4, a6, a1");
		    asm("	shl2 a4, 16, a1");
		    asm("	unpkbu4.l a0, a3:a2");
		    asm("	unpkbu4.s a0, a3:a2");
		    asm("	unpkh2.l a0, a3:a2");
		    asm("	unpkh2.s a0, a3:a2");
		    asm("	unpkhu2.l a0, a3:a2");
		    asm("	unpkhu2.s a0, a3:a2");

		    asm("	ccmatmpy a7:a6, a3:a2:a1:a0, a11:a10:a9:a8");
		    asm("	ccmatmpyr1 a7:a6, a3:a2:a1:a0, a9:a8");
		    asm("	ccmpy32r1 a5:a4, a9:a8, a1:a0");
		    asm("	cmatmpy a7:a6, a3:a2:a1:a0, a11:a10:a9:a8");
		    asm("	cmatmpyr1 a7:a6, a3:a2:a1:a0, a9:a8");
		    asm("	cmpy32r1 a5:a4, a9:a8, a1:a0");
		    asm("	cmpysp a5:a4, a7:a6, a15:a14:a13:a12");
		    asm("	qmpy32 a15:a14:a13:a12, a15:a14:a13:a12, a15:a14:a13:a12");
		    asm("	qmpysp a15:a14:a13:a12, a15:a14:a13:a12, a15:a14:a13:a12");
		    asm("	qsmpy32r1 a15:a14:a13:a12, a15:a14:a13:a12, a15:a14:a13:a12");
		    asm("	dsmpy2 a5:a4, a7:a6, a15:a14:a13:a12");
		    asm("	dmpysu4 a5:a4, a7:a6, a15:a14:a13:a12");
		    asm("	dmpyu2 a5:a4, a7:a6, a15:a14:a13:a12");
		    asm("	dmpyu4 a5:a4, a7:a6, a15:a14:a13:a12");
		    asm("	dmpy2 a5:a4, a7:a6, a15:a14:a13:a12");
		    asm("	dccmpy a5:a4, a7:a6, a15:a14:a13:a12");
		    asm("	dcmpy a5:a4, a7:a6, a15:a14:a13:a12");
		    asm("	mfence");
		}

	}
}

Int main()
{
	Hwi_Params hwiParams;
	Error_Block eb;
	Task_Params taskParams;

	Task_Handle task0;

	Task_Params_init(&taskParams);
	taskParams.stackSize = 2048;
	taskParams.priority = 15;
	Error_init(&eb);
	task0 = Task_create((Task_FuncPtr)myTask, &taskParams, &eb);

	GDB_server_init(4, 4, 0, 0);

	//while(1);

    BIOS_start();    // does not return
    return(0);
}
