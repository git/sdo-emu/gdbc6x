set pagination off                                                     
set logging file gdb_core_test.txt                                           
set logging on                                                         
                                                                       
# sanity testing                                                       
                                                                       
echo Sanity Testing the DSP connections...\n                              
tar rem /dev/gdbtty7                                                   
echo DSP 7:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty6                                                   
echo DSP 6:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty5                                                   
echo DSP 5:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty4                                                   
echo DSP 4:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty3                                                   
echo DSP 3:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty2                                                   
echo DSP 2:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty1                                                   
echo DSP 1:                                                            
info reg DNUM                                                          
                                                                       
tar rem /dev/gdbtty0                                                   
echo DSP 0:                                                            
info reg DNUM

add-symbol-file gdbservertest.out 0x800000

echo \nTesting Breakpoints...\n
b funcCall
b funcCall2
b dostuff

c
info reg PC
c
info reg PC
c
info reg PC

echo \nTesting assembly stepping...\n
d
b funcCall
c
info reg PC

si
info reg PC
si
info reg PC
si
info reg PC

echo \nTesting source stepping...\n
c
info reg PC
s
info reg PC
s 
info reg PC
s
info reg PC
s
info reg PC
s
info reg PC

echo \nTesting step over...\n
d
b main.c:37
c
info reg PC
n
info reg PC

d
b main.c:48
c
info reg PC
n
info reg PC
d

echo \nTesting memory read/write...\n
set *0x0c000010 = 0x0
set *0x0c000014 = 0x4
set *0x0c000018 = 0x8
set *0x0c00001c = 0xc
x/4xw 0x0c000010
set *0x0c000010 = 0x10
set *0x0c000014 = 0x14
set *0x0c000018 = 0x18
set *0x0c00001c = 0x1c
x/4xw 0x0c000010

echo \nTesting disassembly...\n
b 68
c
disas
d

echo \nTesting backtracing...\n
b dostuff
c
bt













