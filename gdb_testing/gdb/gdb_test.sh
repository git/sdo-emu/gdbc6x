#!/bin/bash
app=$1

# set this for debug
#set -x

if [ -e gdb_test.txt ]
then
     rm gdb_test.txt
fi

if [ -e gdb_core_test.txt ]
then
     rm gdb_core_test.txt
fi

# load test app to all DSPs
for (( i=0; i<8; i++ ))
do
     mpmcl reset dsp$i
done

for (( i=0; i<8; i++ ))
do
     mpmcl load dsp$i $app
done

for (( i=0; i<8; i++ ))
do
     mpmcl run dsp$i
done

# open 8 simulatneous GDB sessions and ensure all sessions connect
#gdb6x -iex "tar rem /dev/gdbtty0" -q >> gdb_test.txt &
#PID1=$!
#gdb6x -iex "tar rem /dev/gdbtty1" -q >> gdb_test.txt &
#PID2=$!
#gdb6x -iex "tar rem /dev/gdbtty2" -q >> gdb_test.txt &
#PID3=$!
#gdb6x -iex "tar rem /dev/gdbtty3" -q >> gdb_test.txt &
#PID4=$!
#gdb6x -iex "tar rem /dev/gdbtty4" -q >> gdb_test.txt &
#PID5=$!
#gdb6x -iex "tar rem /dev/gdbtty5" -q >> gdb_test.txt &
#PID6=$!
#gdb6x -iex "tar rem /dev/gdbtty6" -q >> gdb_test.txt &
#PID7=$!
#gdb6x -iex "tar rem /dev/gdbtty7" -q >> gdb_test.txt &
#PID7=$!

#sleep 5

# open the gdb session to finish the testing
gdb6x --batch-silent --command=gdb_test.gdb








