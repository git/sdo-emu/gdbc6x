/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _AET_H
#define _AET_H

#include "Includes.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Minimum unit = 1 byte */

/**************************************************************************\
* Register Overlay Structure
\**************************************************************************/
typedef struct  {
    volatile int32_t IAR_ADD;
    volatile int32_t IAR_DAT;
    volatile int32_t DP_EVT;
    volatile int32_t PID_RD;
    volatile int32_t PC;
    volatile int32_t SPL_PC;
    volatile int32_t SPL_STAT;
    volatile uint8_t RSVD0[244];
    volatile int32_t PIN_MGR_0;
    volatile int32_t PIN_MGR_1;
    volatile int32_t PIN_MGR_2;
    volatile int32_t TP_CNTL;
} CSL_Aet_mmrRegs;

/**
 * AET setup for C66x GDB: set up EMU1 to AET RTOS INT trigger
 */
int AET_C66x_GDB_Setup (void);

/**
 * AET release for C66x GDB
 */
int AET_C66x_GDB_Release (void);

/**
 * AET RTOS INT re-arm for C66x GDB
 */
void AET_C66x_GDB_Intr_Rearm (void);


#ifdef __cplusplus
}
#endif


#endif
