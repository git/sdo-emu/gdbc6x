/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MONITOR_H_
#define MONITOR_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "Includes.h"
#include "Communications.h"
#include "Util.h"
#include "Error.h"
#include <string.h>
#include <stdio.h>


#define NUM_CORE_REGISTERS   66                           /**< DSP Registers to be cached                                              */
#define MAX_PACKET_SIZE     (NUM_CORE_REGISTERS * 8) + 4  /**< Maximum packet size supported includes checksum and starting character  */
#define REASON_HALT_BP_STEP   1                           /**< Halted due to a step or breakpoint                                      */
#define INT_SRC_NOT_AINT      0                           /**< The source of the call is not the AINT                                  */
#define INT_SRC_AINT          1                           /**< The source of the call is the AINT                                      */

/**
 * Breakpoint attributes - these must be mutually exclusive
 */
#define BP_ATTRIBUTE_SET           0x00000001   /**< breakpoint set flag    */
#define BP_ATTRIBUTE_16BIT         0x00000002   /**< 16 bit breakpoint flag */
#define BP_ATTRIBUTE_32BIT         0x00000004   /**< 32 bit breakpoint flag */

#define SWBP_32BIT       0x1001E000   /**< 32 bit SWBP opcode                                   */
#define SWBP_16BIT       0x3C6F       /**< 16 bit SWBP opcode                                   */
#define NUM_BREAKPOINTS  10           /**< The number of breakpoints supported                  */
#define BP32_MASK        0xFF83FFFC   /**< 32 bit breakpoint mask                               */
#define BP16_MASK        0x3C7F       /**< 16 bit breakpoint mask                               */


/* Taken from include/gdb/signals.def from gnu 7.4 sources.  */
typedef enum {
    SIGNAL_0 = 0,
    SIGNAL_HUP = 1,
    SIGNAL_INT = 2,
    SIGNAL_QUIT = 3,
    SIGNAL_ILL = 4,
    SIGNAL_TRAP = 5,
    SIGNAL_ABRT = 6,
    SIGNAL_EMT = 7,
    SIGNAL_FPE = 8,
    SIGNAL_KILL = 9,
    SIGNAL_BUS = 10,
    SIGNAL_SEGV = 11,
    SIGNAL_SYS = 12,
    SIGNAL_PIPE = 13,
    SIGNAL_ALRM = 14,
    SIGNAL_TERM = 15,
    SIGNAL_URG = 16,
    SIGNAL_STOP = 17,
    SIGNAL_TSTP = 18,
    SIGNAL_CONT = 19,
    SIGNAL_CHLD = 20,
    SIGNAL_TTIN = 21,
    SIGNAL_TTOU = 22,
    SIGNAL_IO = 23,
    SIGNAL_XCPU = 24,
    SIGNAL_XFSZ = 25,
    SIGNAL_VTALRM = 26,
    SIGNAL_PROF = 27,
    SIGNAL_WINCH = 28,
    SIGNAL_LOST = 29,
    SIGNAL_USR1 = 30,
    SIGNAL_USR2 = 31,
    SIGNAL_PWR = 32,
/* Similar to SIGIO.  Perhaps they should have the same number.  */
    SIGNAL_POLL = 33,
    SIGNAL_WIND = 34,
    SIGNAL_PHONE = 35,
    SIGNAL_WAITING = 36,
    SIGNAL_LWP = 37,
    SIGNAL_DANGER = 38,
    SIGNAL_GRANT = 39,
    SIGNAL_RETRACT = 40,
    SIGNAL_MSG = 41,
    SIGNAL_SOUND = 42,
    SIGNAL_SAK = 43,
    SIGNAL_PRIO = 44,
    SIGNAL_REALTIME_33 = 45,
    SIGNAL_REALTIME_34 = 46,
    SIGNAL_REALTIME_35 = 47,
    SIGNAL_REALTIME_36 = 48,
    SIGNAL_REALTIME_37 = 49,
    SIGNAL_REALTIME_38 = 50,
    SIGNAL_REALTIME_39 = 51,
    SIGNAL_REALTIME_40 = 52,
    SIGNAL_REALTIME_41 = 53,
    SIGNAL_REALTIME_42 = 54,
    SIGNAL_REALTIME_43 = 55,
    SIGNAL_REALTIME_44 = 56,
    SIGNAL_REALTIME_45 = 57,
    SIGNAL_REALTIME_46 = 58,
    SIGNAL_REALTIME_47 = 59,
    SIGNAL_REALTIME_48 = 60,
    SIGNAL_REALTIME_49 = 61,
    SIGNAL_REALTIME_50 = 62,
    SIGNAL_REALTIME_51 = 63,
    SIGNAL_REALTIME_52 = 64,
    SIGNAL_REALTIME_53 = 65,
    SIGNAL_REALTIME_54 = 66,
    SIGNAL_REALTIME_55 = 67,
    SIGNAL_REALTIME_56 = 68,
    SIGNAL_REALTIME_57 = 69,
    SIGNAL_REALTIME_58 = 70,
    SIGNAL_REALTIME_59 = 71,
    SIGNAL_REALTIME_60 = 72,
    SIGNAL_REALTIME_61 = 73,
    SIGNAL_REALTIME_62 = 74,
    SIGNAL_REALTIME_63 = 75,

/* Used internally by Solaris threads.  See signal(5) on Solaris.  */
    SIGNAL_CANCEL = 76,

/* Yes, this pains me, too.  But LynxOS didn't have SIG32, and now
   GNU/Linux does, and we can't disturb the numbering, since it's
   part of the remote protocol.  Note that in some GDB's
   TARGET_SIGNAL_REALTIME_32 is number 76.  */
    SIGNAL_REALTIME_32 = 77,
/* Yet another pain, IRIX 6 has SIG64. */
    SIGNAL_REALTIME_64 =  78,
/* Yet another pain, GNU/Linux MIPS might go up to 128. */
    SIGNAL_REALTIME_65 = 79,
    SIGNAL_REALTIME_66 = 80,
    SIGNAL_REALTIME_67 = 81,
    SIGNAL_REALTIME_68 = 82,
    SIGNAL_REALTIME_69 = 83,
    SIGNAL_REALTIME_70 = 84,
    SIGNAL_REALTIME_71 = 85,
    SIGNAL_REALTIME_72 = 86,
    SIGNAL_REALTIME_73 = 87,
    SIGNAL_REALTIME_74 = 88,
    SIGNAL_REALTIME_75 = 89,
    SIGNAL_REALTIME_76 = 90,
    SIGNAL_REALTIME_77 = 91,
    SIGNAL_REALTIME_78 = 92,
    SIGNAL_REALTIME_79 = 93,
    SIGNAL_REALTIME_80 = 94,
    SIGNAL_REALTIME_81 = 95,
    SIGNAL_REALTIME_82 = 96,
    SIGNAL_REALTIME_83 = 97,
    SIGNAL_REALTIME_84 = 98,
    SIGNAL_REALTIME_85 = 99,
    SIGNAL_REALTIME_86 = 100,
    SIGNAL_REALTIME_87 = 101,
    SIGNAL_REALTIME_88 = 102,
    SIGNAL_REALTIME_89 = 103,
    SIGNAL_REALTIME_90 = 104,
    SIGNAL_REALTIME_91 = 105,
    SIGNAL_REALTIME_92 = 106,
    SIGNAL_REALTIME_93 = 107,
    SIGNAL_REALTIME_94 = 108,
    SIGNAL_REALTIME_95 = 109,
    SIGNAL_REALTIME_96 = 110,
    SIGNAL_REALTIME_97 = 111,
    SIGNAL_REALTIME_98 = 112,
    SIGNAL_REALTIME_99 = 113,
    SIGNAL_REALTIME_100 = 114,
    SIGNAL_REALTIME_101 = 115,
    SIGNAL_REALTIME_102 = 116,
    SIGNAL_REALTIME_103 = 117,
    SIGNAL_REALTIME_104 = 118,
    SIGNAL_REALTIME_105 = 119,
    SIGNAL_REALTIME_106 = 120,
    SIGNAL_REALTIME_107 = 121,
    SIGNAL_REALTIME_108 = 122,
    SIGNAL_REALTIME_109 = 123,
    SIGNAL_REALTIME_110 = 124,
    SIGNAL_REALTIME_111 = 125,
    SIGNAL_REALTIME_112 = 126,
    SIGNAL_REALTIME_113 = 127,
    SIGNAL_REALTIME_114 = 128,
    SIGNAL_REALTIME_115 = 129,
    SIGNAL_REALTIME_116 = 130,
    SIGNAL_REALTIME_117 = 131,
    SIGNAL_REALTIME_118 = 132,
    SIGNAL_REALTIME_119 = 133,
    SIGNAL_REALTIME_120 = 134,
    SIGNAL_REALTIME_121 = 135,
    SIGNAL_REALTIME_122 = 136,
    SIGNAL_REALTIME_123 = 137,
    SIGNAL_REALTIME_124 = 138,
    SIGNAL_REALTIME_125 = 139,
    SIGNAL_REALTIME_126 = 140,
    SIGNAL_REALTIME_127 = 141,

    SIGNAL_INFO = 142,

/* Some signal we don't know about.  */
    SIGNAL_UNKNOWN = 143,

/* Use whatever signal we use when one is not specifically specified
   (for passing to proceed and so on).  */
    SIGNAL_DEFAULT = 144,

/* Mach exceptions.  In versions of GDB before 5.2, these were just before
   TARGET_SIGNAL_INFO if you were compiling on a Mach host (and missing
   otherwise).  */
    EXC_BAD_ACCESS = 145,
    EXC_BAD_INSTRUCTION = 146,
    EXC_ARITHMETIC = 147,
    EXC_EMULATION = 148,
    EXC_SOFTWARE = 149,
    EXC_BREAKPOINT = 150,

/* If you are adding a new signal, add it just above this comment.  */

/* Last and unused enum value, for sizing arrays, etc.  */
    SIGNAL_LAST = 151
} gdb_signal_t;

/**
 * The DSP registers in GDB order
 */
typedef enum {
	GDB_A0,
	GDB_A1,
	GDB_A2,
	GDB_A3,
	GDB_A4,
	GDB_A5,
	GDB_A6,
	GDB_A7,
	GDB_A8,
	GDB_A9,
	GDB_A10,
	GDB_A11,
	GDB_A12,
	GDB_A13,
	GDB_A14,
	GDB_A15,
	GDB_B0,
	GDB_B1,
	GDB_B2,
	GDB_B3,
	GDB_B4,
	GDB_B5,
	GDB_B6,
	GDB_B7,
	GDB_B8,
	GDB_B9,
	GDB_B10,
	GDB_B11,
	GDB_B12,
	GDB_B13,
	GDB_B14,
	GDB_B15,
	GDB_CSR,
	GDB_PC,
	GDB_A16,
	GDB_A17,
	GDB_A18,
	GDB_A19,
	GDB_A20,
	GDB_A21,
	GDB_A22,
	GDB_A23,
	GDB_A24,
	GDB_A25,
	GDB_A26,
	GDB_A27,
	GDB_A28,
	GDB_A29,
	GDB_A30,
	GDB_A31,
	GDB_B16,
	GDB_B17,
	GDB_B18,
	GDB_B19,
	GDB_B20,
	GDB_B21,
	GDB_B22,
	GDB_B23,
	GDB_B24,
	GDB_B25,
	GDB_B26,
	GDB_B27,
	GDB_B28,
	GDB_B29,
	GDB_B30,
	GDB_B31,
	GDB_TSR,
	GDB_ILC,
	GDB_RILC,
	GDB_AMR,
	GDB_IRP,
	GDB_IFR,
	GDB_NRP,
	GDB_ISR,
	GDB_ICR,
	GDB_IER,
	GDB_ISTP,
	GDB_IN,
	GDB_OUT,
	GDB_ACR,
	GDB_ADR,
	GDB_FADCR,
	GDB_FAUCR,
	GDB_FMCR,
	GDB_GFPGFR,
	GDB_DIER,
	GDB_REP,
	GDB_TSCL,
	GDB_TSCH,
	GDB_ARP,
	GDB_DNUM,
	GDB_SSR,
	GDB_GPLYA,
	GDB_GPLYB,
	GDB_ITSR,
	GDB_NTSR,
	GDB_EFR,
	GDB_IERR,
	GDB_DMSG,
	GDB_CMSG,
	GDB_DT_DMA_ADDR,
	GDB_DT_DMA_DATA,
	GDB_DT_DMA_CNTL,
	GDB_TCU_CNTL,
	GDB_RTDX_REC_CNTL,
	GDB_RTDX_XMT_CNTL,
	GDB_RTDX_CFG,
	GDB_RTDX_RDATA,
	GDB_RTDX_WDATA,
	GDB_RTDX_RADDR,
	GDB_RTDX_WADDR,
	GDB_MFREG0,
	GDB_DBG_STAT,
	GDB_BRK_EN,
	GDB_HWBP0_CNT,
	GDB_HWBP0,
	GDB_HWBP1,
	GDB_HWBP2,
	GDB_HWBP3,
	GDB_OVERLAY,
	GDB_PC_PROF,
	GDB_ATSR,
	GDB_TRR,
	GDB_TCRR,
	GDB_DESR,
	GDB_DETR,
	GDB_EMPTY,
	GDB_LASTVALUE
} GDBRegister;

/**
 * Data structure for handling breakpoints.
 */
struct Breakpoint_Handler
{
	uint32_t      numBreakpoints;                        /**< The number of breakpoints set                         */
	uint32_t      bpInstructions[NUM_BREAKPOINTS];       /**< The instructions replaced by the software breakpoints */
	uint32_t      bpAddress[NUM_BREAKPOINTS];            /**< The addresses of the software breakpoints             */
	uint32_t      bpAttributes[NUM_BREAKPOINTS];         /**< The addresses of the software breakpoints             */
	bool          useUnsafeBp;                           /**< Whether to use an unsafe breakpoint                   */
};

/**
 * Data structure used by monitor.
 */
struct Mon_Session
{
	uint32_t                  coreRegisterFile[NUM_CORE_REGISTERS];    /**< These are the registers that must be cached           */
	uint32_t                  reasonHalt;                              /**< Save the reason for halt                              */
	bool                      pendingRun;                              /**< Is there a pending run?                               */
	bool                      isHalted;                                /**< Status for DSP Core (halted)                          */
	bool                      isInitialized;                           /**< Initialized flag                                      */
	bool                      nonRestartable;                          /**< Halted at non-restartable point                       */
	struct Breakpoint_Handler bpHandler;                               /**< Handle software breakpoints                           */
};


/**
 * Function for initializing the monitor structure.
 */
int Monitor_initialize(struct Mon_Session *monSession);

/**
 * Function for processing RSP packets.
 */
int Monitor_processPacket(struct Mon_Session *monSession, struct Packet *packet,
		                         int maxSize, bool *sendResponse, int interruptSource);

/**
 * Read the pendingRun member of Mon_Session
 */
bool Monitor_getPendingRun(struct Mon_Session *monSession);

/**
 * Reset the pending run member of Mon_Session
 */
void Monitor_resetPendingRun(struct Mon_Session *monSession);

/**
 * Read the isHalted member of Mon_Session
 */
bool Monitor_getIsHalted(struct Mon_Session *monSession);

/**
 * Set the isHalted member of Mon_Session
 */
void Monitor_setIsHalted(struct Mon_Session *monSession);

/**
 * Set the reasonforHalt member of Mon_Session to  REASON_HALT_BP_STEP
 */
void Monitor_setReasonforHaltBPStep(struct Mon_Session *monSession);

/**
 * Check for the halted in non-interruptible code condition
 */
bool Monitor_checkNonRestartable(void);

/**
 * Function for setting software breakpoints.
 */
int setSWBP(struct Mon_Session *monSession, uint32_t breakAddr, int breakLength);

/**
 * Function for removing software breakpoints.
 */
void removeSWBP(struct Mon_Session *monSession, uint32_t breakAddr);

/**
 * Function for clearing all software breakpoints.
 */
void clearAllSWBP(struct Mon_Session *monSession);



#ifdef __cplusplus
}
#endif /* extern "C" */

#endif /* MONITOR_H_ */
