/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef COMMUNICATIONS_H_
#define COMMUNICATIONS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "Includes.h"
#include "Util.h"
#include "Error.h"

/**
 * Not all commands stated here are
 * supported in this implementation of GDB Server.
 */
typedef enum {
    READ_ALL_REGISTERS_CMD,     /**< g               */
    WRITE_ALL_REGISTERS_CMD,    /**< G               */
    READ_MEMORY_CMD,            /**< m               */
    WRITE_MEMORY_CMD,           /**< M               */
    WRITE_MEMORY_BINARY_CMD,    /**< X               */
    STEP_CMD,                   /**< s               */
    CONTINUE_CMD,               /**< c               */
    STATUS_CMD,                 /**< ?               */
    SET_BREAKPOINT_CMD,         /**< Z               */
    REMOVE_BREAKPOINT_CMD,      /**< z               */
    READ_ONE_REGISTER_CMD,      /**< p               */
    WRITE_ONE_REGISTER_CMD,     /**< P               */
    EXTENDED_CMD,               /**< !               */
    RESTART_CMD,                /**< R               */
    QXFER_CMD,                  /**< qXfer           */
    QUERY_CMD,                  /**< q               */
    FILEIO_CMD,                 /**< F               */
    DETACH_CMD,                 /**< D               */
    QNONSTOP_CMD,               /**< QNonStop        */
    NOACK_MODE_CMD,             /**< QStartNoAckMode */
    SET_THREAD_CMD,             /**< H               */
    VCONT_QUERY_CMD,            /**< vCont?          */
    VCONT_CMD,                  /**< vCont           */
    HALT_CMD,                   /**< 0x03            */
    KILL_CMD,                   /**< k               */
    UNKNOWN_CMD
} GDBCommandId;


/**
 * Data structure used for populating the fields necessary
 * for communication.
 */
struct Comm_Session
{
	uint32_t sharedAddress;      /**< Shared address used for initialization                */
	uint32_t sharedAddressComm;  /**< Shared address used for reading RSP packets           */
	uint32_t readyAddr;          /**< Signal to ARM that a message is ready to be serviced  */
	int      intNum;             /**< Interrupt number used for communication               */
	int      isInitialized;      /**< Initialized flag - 2 step processes                   */
};

/**
 * Data structure used for receiving and RSP packet
 */
struct Packet
{
	GDBCommandId commandId;         /**< The command type                */
	char         *receiveData;      /**< Pointer to the received data    */
	char         *sendData;         /**< Pointer to the data to send     */
	int          receiveLength;     /**< Length of received data section */
	int          sendLength;        /**< Length of sent data section     */
};

/**
 * Initialize the communication with the ARM core
 */
int Comm_initialize(struct Comm_Session *commSession);

/**
 * Get an RSP packet
 */
int Comm_getPacket(struct Comm_Session *commSession, struct Packet *packet, int maxPacketSize);

/**
 * Send an RSP packet
 */
int Comm_sendPacket(struct Comm_Session *commSession, struct Packet *packet, bool response);

/**
 * Send a status packet for the kernel module
 */
void Comm_sendStatus(struct Comm_Session *commSession, uint32_t packet);

/**
 * Set the communications interrupt
 */
void Comm_setCommInterrupt(struct Comm_Session *commSession);

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif /* COMMUNICATIONS_H_ */
