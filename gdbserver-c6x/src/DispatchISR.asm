;/*
;* Copyright (c) 2014, Texas Instruments Incorporated
; * All rights reserved.
; *
; * Redistribution and use in source and binary forms, with or without
; * modification, are permitted provided that the following conditions
; * are met:
; *
; * *  Redistributions of source code must retain the above copyright
; *    notice, this list of conditions and the following disclaimer.
; *
; * *  Redistributions in binary form must reproduce the above copyright
; *    notice, this list of conditions and the following disclaimer in the
; *    documentation and/or other materials provided with the distribution.
; *
; * *  Neither the name of Texas Instruments Incorporated nor the names of
; *    its contributors may be used to endorse or promote products derived
; *    from this software without specific prior written permission.
; *
; * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
; * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
; * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
; * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
; * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
; * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
; * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
; * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
; * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
; * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
; */

;/**
; * FUNCTION NAME: dispatchISR
; *
; * DESCRIPTION: This function handles the context saving/restoring for
; *              the communications interrupt. On a continue or step command
; *              from GDB, the context is restored from the saved cache,
; *              otherwise it is restored from the stack.
; *
; * ARGUMENTS:	None
; */
	.global dispatchISR
	.global gdb_globalData
	.global Load_monSession
	.global handleComm
	.global Monitor_getPendingRun
	.global Monitor_resetPendingRun
dispatchISR:
	;save all necessary registers in the stack
	STW B31,*B15--[1]
	STW B30,*B15--[1]
	STW B29,*B15--[1]
	STW B28,*B15--[1]
	STW B27,*B15--[1]
	STW B26,*B15--[1]
	STW B25,*B15--[1]
	STW B24,*B15--[1]
	STW B23,*B15--[1]
	STW B22,*B15--[1]
	STW B21,*B15--[1]
	STW B20,*B15--[1]
	STW B19,*B15--[1]
	STW B18,*B15--[1]
	STW B17,*B15--[1]
	STW B16,*B15--[1]
	STW A31,*B15--[1]
	STW A30,*B15--[1]
	STW A29,*B15--[1]
	STW A28,*B15--[1]
	STW A27,*B15--[1]
	STW A26,*B15--[1]
	STW A25,*B15--[1]
	STW A24,*B15--[1]
	STW A23,*B15--[1]
	STW A22,*B15--[1]
	STW A21,*B15--[1]
	STW A20,*B15--[1]
	STW A19,*B15--[1]
	STW A18,*B15--[1]
	STW A17,*B15--[1]
	STW A16,*B15--[1]
	STW B9,*B15--[1]
	STW B8,*B15--[1]
	STW B7,*B15--[1]
	STW B6,*B15--[1]
	STW B5,*B15--[1]
	STW B4,*B15--[1]
	STW B3,*B15--[1]
	STW B2,*B15--[1]
	STW B1,*B15--[1]
	STW B0,*B15--[1]
	STW A9,*B15--[1]
	STW A8,*B15--[1]
	STW A7,*B15--[1]
	STW A6,*B15--[1]
	STW A5,*B15--[1]
	STW A4,*B15--[1]
	STW A3,*B15--[1]
	STW A2,*B15--[1]
	STW A1,*B15--[1]
	STW A0,*B15--[1]

	;Save the ILC, RILC, and ITSR
	MVC ILC, B0
	STW B0,*B15--[1]

	MVC RILC, B0
	STW B0,*B15--[1]

	MVC ITSR, B0
	STW B0,*B15--[1]

	;handle the command
	CALLP handleComm, B3

	;Load the monitor data structure
	CALLP Load_monSession, B3

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;identify if the core is set
	;to run
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	;Load the pendingRun variable
	;Use calling convention - return value and
	;argument passed in A4, B3 contains return
	;pointer
	CALLP Monitor_getPendingRun, B3

	;if there is no pending run restore from stack
	;otherwise we must restore from register file
	MV A4, B0
	[!B0] BNOP restoreFromStack
	nop 5

	;reset the pendingRun variable
	CALLP Load_monSession, B3
	CALLP Monitor_resetPendingRun, B3

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;restore the registers from the
	;register file
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;restore the pointer to monSession
	CALLP Load_monSession, B3
	MV A4, B0
	LDW *B0++, A0
	LDW *B0++, A1
	LDW *B0++, A2
	LDW *B0++, A3
	LDW *B0++, A4
	LDW *B0++, A5
	LDW *B0++, A6
	LDW *B0++, A7
	LDW *B0++, A8
	LDW *B0++, A9
	LDW *B0++, A10
	LDW *B0++, A11
	LDW *B0++, A12
	LDW *B0++, A13
	LDW *B0++, A14
	;skip B0 and B1 for now
	LDW *B0++[3], A15
	LDW *B0++, B2
	LDW *B0++, B3
	LDW *B0++, B4
	LDW *B0++, B5
	LDW *B0++, B6
	LDW *B0++, B7
	LDW *B0++, B8
	LDW *B0++, B9
	LDW *B0++, B10
	LDW *B0++, B11
	LDW *B0++, B12
	LDW *B0++, B13
	;we will not modify the stack pointer
	LDW *B0++[2], B14

	;restore the CSR
	;we will not modify the PC
	LDW *B0++[2], B1
	nop 5
	MVC B1, CSR

	LDW *B0++, A16
	LDW *B0++, A17
	LDW *B0++, A18
	LDW *B0++, A19
	LDW *B0++, A20
	LDW *B0++, A21
	LDW *B0++, A22
	LDW *B0++, A23
	LDW *B0++, A24
	LDW *B0++, A25
	LDW *B0++, A26
	LDW *B0++, A27
	LDW *B0++, A28
	LDW *B0++, A29
	LDW *B0++, A30
	LDW *B0++, A31

	LDW *B0++, B16
	LDW *B0++, B17
	LDW *B0++, B18
	LDW *B0++, B19
	LDW *B0++, B20
	LDW *B0++, B21
	LDW *B0++, B22
	LDW *B0++, B23
	LDW *B0++, B24
	LDW *B0++, B25
	LDW *B0++, B26
	LDW *B0++, B27
	LDW *B0++, B28
	LDW *B0++, B29
	LDW *B0++, B30
	LDW *B0, B31
	nop 5

	;restore the ILC, RILC, ITSR
	LDW *++B15[1], B1
	nop 5
	MVC B1, ITSR

	LDW *++B15[1], B1
	nop 5
	MVC B1, RILC

	LDW *++B15[1], B1
	nop 5
	MVC B1, ILC

	;restore B0 and B1
	;we will do this "by hand" in order
	;to not overwrite other registers
	ADDK -192, B0

	LDW *B0, B1
	LDW *-B0[1], B0

	;restore the stack pointer
	ADDK 208, B15

	;branch to return pointer
	B IRP
	nop 5

restoreFromStack:
;restore the ILC, RILC, ITSR
	LDW *++B15[1], B0
	nop 5
	MVC B0, ITSR

	LDW *++B15[1], B0
	nop 5
	MVC B0, RILC

	LDW *++B15[1], B0
	nop 5
	MVC B0, ILC

	LDW *++B15[1], A0
	LDW *++B15[1], A1
	LDW *++B15[1], A2
	LDW *++B15[1], A3
	LDW *++B15[1], A4
	LDW *++B15[1], A5
	LDW *++B15[1], A6
	LDW *++B15[1], A7
	LDW *++B15[1], A8
	LDW *++B15[1], A9
	LDW *++B15[1], B0
	LDW *++B15[1], B1
	LDW *++B15[1], B2
	LDW *++B15[1], B3
	LDW *++B15[1], B4
	LDW *++B15[1], B5
	LDW *++B15[1], B6
	LDW *++B15[1], B7
	LDW *++B15[1], B8
	LDW *++B15[1], B9
	LDW *++B15[1], A16
	LDW *++B15[1], A17
	LDW *++B15[1], A18
	LDW *++B15[1], A19
	LDW *++B15[1], A20
	LDW *++B15[1], A21
	LDW *++B15[1], A22
	LDW *++B15[1], A23
	LDW *++B15[1], A24
	LDW *++B15[1], A25
	LDW *++B15[1], A26
	LDW *++B15[1], A27
	LDW *++B15[1], A28
	LDW *++B15[1], A29
	LDW *++B15[1], A30
	LDW *++B15[1], A31
	LDW *++B15[1], B16
	LDW *++B15[1], B17
	LDW *++B15[1], B18
	LDW *++B15[1], B19
	LDW *++B15[1], B20
	LDW *++B15[1], B21
	LDW *++B15[1], B22
	LDW *++B15[1], B23
	LDW *++B15[1], B24
	LDW *++B15[1], B25
	LDW *++B15[1], B26
	LDW *++B15[1], B27
	LDW *++B15[1], B28
	LDW *++B15[1], B29
	LDW *++B15[1], B30
	LDW *++B15[1], B31

	;branch to return pointer
	B IRP
	NOP 5


