/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SERVER_H_
#define SERVER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "Includes.h"
#include "Util.h"
#include "Monitor.h"
#include "Communications.h"
#include <ti/sysbios/family/c64p/Hwi.h>

#define AET_RTOS_INT	           9           /**< The event ID for AET RTOS interrupt */
#define DETACH_COMM            0x10000010    /**< The detach completely from ARM signal              */
#define AINT_NUM        	   2             /**< The analysis interrupt number                      */
#define STATUS_NEW_PACKET      0x00000010    /**< New packet has been written to DSP                 */

/**
 * This function is used for communication to the ARM
 */
void handleComm(void);

/**
 * This function is used for handling the non-Restartable condition
 */
void nonRestartable(void);

/**
 * This function handles the context saving/restoring for
 * the communications interrupt. On a continue or step command
 * from GDB, the context is restored from the saved cache,
 * otherwise it is restored from the stack.
 */
void dispatchISR(void);

/**
 * This function saves the current state of the DSP,
 * and sets up the DSP to service commands from the ARM
 */
void debug_Preamble(void);

/**
 * This function loads the monitor session for use
 * by the ISRs
 */
struct Mon_Session* Load_monSession(void);

/**
 * This function loads the communication session for use
 * by the ISRs
 */
struct Comm_Session* Load_commSession(void);

#ifdef __cplusplus
}
#endif /* extern "C" */


#endif /* SERVER_H_ */
