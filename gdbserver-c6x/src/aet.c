/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "aet.h"
#include "c6x.h"
#include "Error.h"

/* 
 Base Address for AET Memory Mapped Registers
 */
CSL_Aet_mmrRegs *AET_mmrRegs = (CSL_Aet_mmrRegs*)0x1bc0020;

extern int AET_C66x_GDB_Setup (void)
{
	//Setup TCU_CNTL
	//Clear AET interrupt flag in TCU_CNTL register
	TCU_CNTL = TCU_CNTL | 0x00A00000;

	//Enable AET interrupt and EMU1 trigger in TCU_CNTL register
	if((TCU_CNTL & 0x00028042) != 0x00028042)
	{
		TCU_CNTL = 0x1;
		TCU_CNTL = 0x000A0000;
		TCU_CNTL = 0x00028042;
	}

	AET_mmrRegs->PIN_MGR_0 = 0x44;

	//Claim AET HW

	/* Write a 1 to the PID_RD register */
	AET_mmrRegs->PID_RD = 0x1;

	/*
	 If the claim worked correctly, bit 0 and 1 should be set, so read it back
	 and see
	 */
	if ((AET_mmrRegs->PID_RD & 0x00000003) == 1)
	{
		//do nothing
	} /* end if */
	else
	{
	 return ERR_AET_FNOCLAIM;
	}/* end else */

	//Setup AET job

	AET_mmrRegs->IAR_ADD = 0x87; //CIS_BUS_SEL
	AET_mmrRegs->IAR_DAT = 0x20000;

	AET_mmrRegs->IAR_ADD = 0x8A; //AUX_EVT_1_CNTL
	AET_mmrRegs->IAR_DAT = 0x644;

	AET_mmrRegs->IAR_ADD = 0x8B; //AUX_1_ENA
	AET_mmrRegs->IAR_DAT = 0x80;

	AET_mmrRegs->IAR_ADD = 0x8A; //AUX_EVT_1_CNTL
	AET_mmrRegs->IAR_DAT = 0x644;

	AET_mmrRegs->IAR_ADD = 0x86; //FUNC_CNTL#include "c6x.h"
	AET_mmrRegs->IAR_DAT = 0x1b000000;

	AET_mmrRegs->IAR_ADD = 0x87; //CIS_BUS_SEL
	AET_mmrRegs->IAR_DAT = 0x20000;

	AET_mmrRegs->IAR_ADD = 0x84; //TB_ENA
	AET_mmrRegs->IAR_DAT = 0x0;

	AET_mmrRegs->IAR_ADD = 0x140; //TB_1W_0_ORS
	AET_mmrRegs->IAR_DAT = 0x800;

	AET_mmrRegs->IAR_ADD = 0x141; //TB_1W_0_CNTL
	AET_mmrRegs->IAR_DAT = 0x4800aaaa;

	AET_mmrRegs->IAR_ADD = 0x85; //TB_DM
	AET_mmrRegs->IAR_DAT = 0x0;

	AET_mmrRegs->IAR_ADD = 0x84; //TB_ENA
	AET_mmrRegs->IAR_DAT = 0x1;

	//Enable AET
	/* Enable the AET Unit */
	AET_mmrRegs->PID_RD = 0x2;

	if ((AET_mmrRegs->PID_RD & 0x00000003) == 1)
	{
		/* Can't Check, so assume we passed */
	}

	return NO_ERR;
}

extern int AET_C66x_GDB_Release (void)
{
	// disable TCU_CNTL
	TCU_CNTL = 0x0;	

	AET_mmrRegs->IAR_ADD = 0x84; //TB_ENA
	AET_mmrRegs->IAR_DAT = 0x0;

	AET_mmrRegs->IAR_ADD = 0x140; //TB_1W_0_ORS
	AET_mmrRegs->IAR_DAT = 0x0;

	AET_mmrRegs->IAR_ADD = 0x141; //TB_1W_0_CNTL
	AET_mmrRegs->IAR_DAT = 0x0;

	AET_mmrRegs->IAR_ADD = 0x86; //FUNC_CNTL
	AET_mmrRegs->IAR_DAT = 0x1b000000;

	/* Release the AET Unit */
	AET_mmrRegs->PID_RD = 0x0;

	if ((AET_mmrRegs->PID_RD & 0x00000003) != 1)
	{
		//do nothing
	} /* end if */
	else
	{
		return ERR_AET_FNORELEASE;
	} /* end else */

	return NO_ERR;
}

extern void AET_C66x_GDB_Intr_Rearm (void)
{
	//Clear AET interrupt flag in TCU_CNTL register to re-arm the AET interrupt
	TCU_CNTL = TCU_CNTL | 0x00A00000;

	//Reset EMU1 pulse catcher
	TCU_CNTL = TCU_CNTL & 0xFFFFFFBF;
	TCU_CNTL = TCU_CNTL | 0x00000040;
}
