/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ERROR_H_
#define ERROR_H_

#ifdef __cplusplus
extern "C" {
#endif

#define NO_ERR                           0  /**< No error */
#define ERR_AET_FNOCLAIM                -1  /**< Could not Claim AET Unit */
#define ERR_AET_FNORELEASE              -2  /**< Failed to Release AET Unit */
#define ERR_INIT_COMM                   -3	/**< Communication was attempted before initialization       */
#define ERR_INT_OUT_RANGE               -4  /**< Interrupt number is out of range                        */
#define ERR_ARM_DSP_IPC_INIT            -5  /**< ARM-DSP IPC initialization error                     */
#define ERR_HWI_CREATE                  -6  /**< Could not create HWI (BIOS error)                       */
#define ERR_STARTING_CHAR               -7  /**< Could not find the starting character for debug message */
#define ERR_MONITOR_CMD                 -8  /**< The command RSP pay load is null                        */
#define ERR_MONITOR_NULL_PKT            -9  /**< The buffer to be filled with the response is NULL       */
#define ERR_COMM_WRONG_DSP             -10  /**< The DSP number requested does not match                 */
#define ERR_COMM_INIT_FAIL             -11  /**< Could not initialize the communication with ARM         */
#define ERR_REGISTER_RANGE             -12  /**< Register read/write that was attempted was out of range */
#define ERR_BUFF_WRITE                 -13  /**< Could not completely write register value to buffer     */
#define ERR_FORMAT                     -14  /**< The packet is formatted incorrectly                     */
#define ERR_REG_READ_WRITE             -15  /**< Could not read/write the register                       */
#define ERR_NO_BP                      -16  /**< Could not set software breakpoint                       */
#define ERR_HALTED_AT_NONRESTARTABLE   -17  /**< Halted in a non-restartable code                        */
#define ERR_MFREG0_NOT_CLAIMED         -18  /**< Could not claim MFREG0                                  */
#define ERR_GLOB_NOT_CALLED            -19  /**< Global initialization has not occured                   */

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif /* ERROR_H_ */
