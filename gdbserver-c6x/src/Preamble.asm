;/*
;* Copyright (c) 2014, Texas Instruments Incorporated
; * All rights reserved.
; *
; * Redistribution and use in source and binary forms, with or without
; * modification, are permitted provided that the following conditions
; * are met:
; *
; * *  Redistributions of source code must retain the above copyright
; *    notice, this list of conditions and the following disclaimer.
; *
; * *  Redistributions in binary form must reproduce the above copyright
; *    notice, this list of conditions and the following disclaimer in the
; *    documentation and/or other materials provided with the distribution.
; *
; * *  Neither the name of Texas Instruments Incorporated nor the names of
; *    its contributors may be used to endorse or promote products derived
; *    from this software without specific prior written permission.
; *
; * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
; * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
; * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
; * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
; * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
; * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
; * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
; * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
; * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
; * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
; */

;/**
; * FUNCTION NAME: debug_Preamble
; *
; * DESCRIPTION: This function saves the current state of the core,
; *              and sets up the DSP to service commands from the ARM.
; *              It is the AINT isr.
; *
; * ARGUMENTS:	None
; */
	.global debug_Preamble
	.global gdb_globalData
	.global nonRestartable
	.global Monitor_getIsHalted
	.global Monitor_setIsHalted
	.global Monitor_setReasonforHaltBPStep
	.global Monitor_checkNonRestartable
	.global Load_monSession
	.global Load_commSession
	.global Comm_setCommInterrupt
debug_Preamble:
	;save all necessary registers in the stack
	STW B31,*B15--[1]
	STW B30,*B15--[1]
	STW B29,*B15--[1]
	STW B28,*B15--[1]
	STW B27,*B15--[1]
	STW B26,*B15--[1]
	STW B25,*B15--[1]
	STW B24,*B15--[1]
	STW B23,*B15--[1]
	STW B22,*B15--[1]
	STW B21,*B15--[1]
	STW B20,*B15--[1]
	STW B19,*B15--[1]
	STW B18,*B15--[1]
	STW B17,*B15--[1]
	STW B16,*B15--[1]
	STW A31,*B15--[1]
	STW A30,*B15--[1]
	STW A29,*B15--[1]
	STW A28,*B15--[1]
	STW A27,*B15--[1]
	STW A26,*B15--[1]
	STW A25,*B15--[1]
	STW A24,*B15--[1]
	STW A23,*B15--[1]
	STW A22,*B15--[1]
	STW A21,*B15--[1]
	STW A20,*B15--[1]
	STW A19,*B15--[1]
	STW A18,*B15--[1]
	STW A17,*B15--[1]
	STW A16,*B15--[1]
	STW B9,*B15--[1]
	STW B8,*B15--[1]
	STW B7,*B15--[1]
	STW B6,*B15--[1]
	STW B5,*B15--[1]
	STW B4,*B15--[1]
	STW B3,*B15--[1]
	STW B2,*B15--[1]
	STW B1,*B15--[1]
	STW B0,*B15--[1]
	STW A9,*B15--[1]
	STW A8,*B15--[1]
	STW A7,*B15--[1]
	STW A6,*B15--[1]
	STW A5,*B15--[1]
	STW A4,*B15--[1]
	STW A3,*B15--[1]
	STW A2,*B15--[1]
	STW A1,*B15--[1]
	STW A0,*B15--[1]

	;Save the ILC, RILC, and ITSR
	MVC ILC, B0
	STW B0,*B15--[1]

	MVC RILC, B0
	STW B0,*B15--[1]

	MVC ITSR, B0
	STW B0,*B15--[1]


	;load the address of the
	;monitor session data structure
	CALLP Load_monSession, B3
	MV A4, B0

	;save the registers in the structure
	;ensure that we pull from the stack
	;save A0-A4
	LDW *+B15[4], A0
	LDW *+B15[5], A0
	LDW *+B15[6], A0
	LDW *+B15[7], A0
	LDW *+B15[8], A0
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++

	;save A5-A9
	LDW *+B15[9], A0
	LDW *+B15[10], A0
	LDW *+B15[11], A0
	LDW *+B15[12], A0
	LDW *+B15[13], A0
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++

	;save A10-A15
	;these are callee save registers
	STW A10, *B0++
	STW A11, *B0++
	STW A12, *B0++
	STW A13, *B0++
	STW A14, *B0++
	STW A15, *B0++

	;save B0-B4
	LDW *+B15[14], A0
	LDW *+B15[15], A0
	LDW *+B15[16], A0
	LDW *+B15[17], A0
	LDW *+B15[18], A0
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++

	;save B5-B9
	LDW *+B15[19], A0
	LDW *+B15[20], A0
	LDW *+B15[21], A0
	LDW *+B15[22], A0
	LDW *+B15[23], A0
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++

	;save B10-B14
	;these are callee save registers
	STW B10, *B0++
	STW B11, *B0++
	STW B12, *B0++
	STW B13, *B0++
	STW B14, *B0++

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;Save the stack pointer in
	;the structure
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	;read the stack pointer
	MV B15, B1

	;remove the space we have allocated
	ADDK 220, B1,

	;save the value of the stack
	;pointer in the structure
	STW B1, *B0++

	;save the CSR
	MVC CSR, B1
	STW B1, *B0++

	;Save the PC in the structure
	;it will just be stored in the ARP
	MVC ARP, B1
	STW B1, *B0++

	;save A16-A20
	LDW *+B15[24], A0
	LDW *+B15[25], A0
	LDW *+B15[26], A0
	LDW *+B15[27], A0
	LDW *+B15[28], A0
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++

	;save A21-A25
	LDW *+B15[29], A0
	LDW *+B15[30], A0
	LDW *+B15[31], A0
	LDW *+B15[32], A0
	LDW *+B15[33], A0
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++

	;save A26-A30
	LDW *+B15[34], A0
	LDW *+B15[35], A0
	LDW *+B15[36], A0
	LDW *+B15[37], A0
	LDW *+B15[38], A0
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++

	;save A31-B19
	LDW *+B15[39], A0
	LDW *+B15[40], A0
	LDW *+B15[41], A0
	LDW *+B15[42], A0
	LDW *+B15[43], A0
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++

	;save B20-B24
	LDW *+B15[44], A0
	LDW *+B15[45], A0
	LDW *+B15[46], A0
	LDW *+B15[47], A0
	LDW *+B15[48], A0
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++

	;save B25-B29
	LDW *+B15[49], A0
	LDW *+B15[50], A0
	LDW *+B15[51], A0
	LDW *+B15[52], A0
	LDW *+B15[53], A0
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++
	STW A0, *B0++

	;save B30-B31
	LDW *+B15[54], A0
	LDW *+B15[55], A0
	nop 3
	STW A0, *B0++
	STW A0, *B0++

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;Find the source of the halt
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	;load the monSession data structure
	;if the halted flag is not set this
	;means that the halt came from either
	;a step or a breakpoint, not from the
	;user
	CALLP Load_monSession, B3

	;Load the isHalted variable
	;Use calling convention - return value and
	;argument passed in A4, B3 contains return
	;pointer
	CALLP Monitor_getIsHalted, B3

	;Must use B bank for conditional statements
	MV A4, B0
	[B0] BNOP cleanup
	nop 5

handleBPStep:
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;we will set the communications
	;interrupt (DispatchISR), set the
	;halted flag, set the reason for halt and
	;check if we are halted in non-interruptible
	;code
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;set the halted flag
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	CALLP Load_monSession, B3
	CALLP Monitor_setIsHalted, B3

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;set the reason for halt
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	CALLP Load_monSession, B3
	CALLP Monitor_setReasonforHaltBPStep, B3

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;check for halted at non-interruptible code
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	CALLP Load_monSession, B3
	CALLP Monitor_checkNonRestartable, B3

	;check return value
	MV A4, B0
	[!B0] BNOP setCommInterrupt
	nop 5

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;handle the non-restartable condition
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;this call will not return
	CALLP nonRestartable, B3

setCommInterrupt:
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;set the communications interrupt
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	CALLP Load_commSession, B3
	CALLP Comm_setCommInterrupt, B3

cleanup:
	LDW *++B15[1], B0
	nop 5
	MVC B0, ITSR

	LDW *++B15[1], B0
	nop 5
	MVC B0, RILC

	LDW *++B15[1], B0
	nop 5
	MVC B0, ILC

	LDW *++B15[1], A0
	LDW *++B15[1], A1
	LDW *++B15[1], A2
	LDW *++B15[1], A3
	LDW *++B15[1], A4
	LDW *++B15[1], A5
	LDW *++B15[1], A6
	LDW *++B15[1], A7
	LDW *++B15[1], A8
	LDW *++B15[1], A9
	LDW *++B15[1], B0
	LDW *++B15[1], B1
	LDW *++B15[1], B2
	LDW *++B15[1], B3
	LDW *++B15[1], B4
	LDW *++B15[1], B5
	LDW *++B15[1], B6
	LDW *++B15[1], B7
	LDW *++B15[1], B8
	LDW *++B15[1], B9
	LDW *++B15[1], A16
	LDW *++B15[1], A17
	LDW *++B15[1], A18
	LDW *++B15[1], A19
	LDW *++B15[1], A20
	LDW *++B15[1], A21
	LDW *++B15[1], A22
	LDW *++B15[1], A23
	LDW *++B15[1], A24
	LDW *++B15[1], A25
	LDW *++B15[1], A26
	LDW *++B15[1], A27
	LDW *++B15[1], A28
	LDW *++B15[1], A29
	LDW *++B15[1], A30
	LDW *++B15[1], A31
	LDW *++B15[1], B16
	LDW *++B15[1], B17
	LDW *++B15[1], B18
	LDW *++B15[1], B19
	LDW *++B15[1], B20
	LDW *++B15[1], B21
	LDW *++B15[1], B22
	LDW *++B15[1], B23
	LDW *++B15[1], B24
	LDW *++B15[1], B25
	LDW *++B15[1], B26
	LDW *++B15[1], B27
	LDW *++B15[1], B28
	LDW *++B15[1], B29
	LDW *++B15[1], B30
	LDW *++B15[1], B31

	;branch to return pointer
	B ARP
	nop 5




