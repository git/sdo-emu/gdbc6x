/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Monitor.h"

#define OK_RESPONSE     "OK" /**< Response for signaling OK to gdb */
#define REG_READ_FLAG    0   /**< Write specific register          */
#define REG_WRITE_FLAG   1   /**< Read specific register           */

#define ENDIAN           LITTLE_ENDIAN
#define LITTLE_ENDIAN    0
#define BIG_ENDIAN       1

#define BREAKPOINT_TYPE_SW  0

/**
 * Function for swizzling the bytes to target order.
 */
static uint32_t toTargetOrder(uint32_t value, int endian);

/**
 * Function for reading/writing non-cached DSP registers.
 */
static int readWriteReg(uint32_t *value, GDBRegister regNo, int readWrite);

/**
 * FUNCTION NAME: Monitor_initialize
 *
 * DESCRIPTION: Function for initializing the monitor structure.
 *
 * ARGUMENTS:
 * 				Mon_Session *monSession: Pointer to the monitor session to
 * 				                         be initialized
 *
 * RETURN:		Error code
 *
 */
extern int Monitor_initialize(struct Mon_Session *monSession)
{
	memset(monSession, 0, sizeof(struct Mon_Session));
	monSession->isInitialized  = true;
	monSession->bpHandler.useUnsafeBp = true;

	return NO_ERR;
}

/**
 * FUNCTION NAME: Monitor_processPacket
 *
 * DESCRIPTION: This function is used dispatch RSP commands to the proper
 * 				function handlers
 *
 * ARGUMENTS:
 *              Mon_Session   *monSession: Pointer to the monitor session
 * 				Packet            *packet: Pointer to the command to be processed
 * 				int               maxSize: The size (in bytes) allocated to sendPacket
 * 				bool         sendResponse: Whether a response is necessary
 *
 * RETURN:		Error code
 *
 */
extern int Monitor_processPacket(struct Mon_Session *monSession, struct Packet *packet,
		                         int maxSize, bool *sendResponse, int interruptSource)
{
	*sendResponse = true;
	packet->sendLength = 0;

	//go ahead and write the starting character '$'
	packet->sendData[0] = '$';
	packet->sendData++;

	// handle the empty response for an unknown packet
	if(packet->commandId == UNKNOWN_CMD)
	{
		return NO_ERR;
	}

	// ensure space for the buffer has been pre-allocated
	if(packet->sendData == NULL)
	{
		return ERR_MONITOR_NULL_PKT;
	}

	switch(packet->commandId)
	{
		case READ_ALL_REGISTERS_CMD:
		{
			int i = 0;
			// always update the PC in the register file
			// we need to do this because we don't know
			// what the value of it is in the AINT ISR
			if(INT_SRC_AINT == interruptSource)
			{
				// Ensure the last bit is masked off
			    // it is always assumed to be 0
				monSession->coreRegisterFile[GDB_PC] = ARP & 0xFFFFFFFE;
			}
			else
			{
				// Ensure the last bit is masked off
			    // it is always assumed to be 0
				monSession->coreRegisterFile[GDB_PC] = IRP & 0xFFFFFFFE;
			}

			// write all registers
			// only registers 0 - NUM_CORE_REGISTERS will be written in this packet
			for(i = 0; i < NUM_CORE_REGISTERS; i++)
			{
				// each register will fill two words (8 bytes) because we are converting
				// them to characters
				sprintf(packet->sendData + 8*i,"%.8x", toTargetOrder(monSession->coreRegisterFile[i], ENDIAN));
			}
			packet->sendLength = MAX_PACKET_SIZE - 4;
			return NO_ERR;
		}
		case READ_MEMORY_CMD:
		{
			uint32_t addr   = 0;
			uint32_t length = 0;
			uint32_t temp   = 0;
			int error       = NO_ERR;
			int i           = 0;

			// get the length and value of address
			error = sscanf(packet->receiveData,"%x,%x", &addr, &length);

			if(error == EOF)
			{
				// we will simply return no error and leave
				// the send string empty
				return NO_ERR;
			}

			// perform the read byte by byte
			for(i = 0; i < length; i++)
			{
				temp = *(char*)(addr + i);
				packet->sendData[2*i] = hexCharacters[(temp >> 4) & 0xF];
				packet->sendData[2*i + 1] = hexCharacters[temp & 0xF];
			}

			// the format being sent is characters, therefore each
			// nibble will take up one byte
			packet->sendLength = length * 2;
			return NO_ERR;
		}
		case WRITE_MEMORY_CMD:
		{
			uint32_t addr     = 0;
			uint32_t length   = 0;
			char*    data     = 0;
			char     value    = 0;
			int      error    = NO_ERR;
			int      i        = 0;

			// get the length and value of address
			error = sscanf(packet->receiveData,"%x,%x:%s", &addr, &length);

			if(error == EOF)
			{
				// we will simply return no error and leave
				// the send string empty
				return NO_ERR;
			}
			// find the start of the data
			for(i = 0; i < packet->receiveLength; i++)
			{
				if(packet->receiveData[i] == ':')
				{
					data = packet->receiveData + i;
					break;
				}
			}

			if(':' != *data)
			{
				return ERR_FORMAT;
			}
			else
			{
				// move the data pointer past the colon
				data++;
			}

			// perform the write, byte by byte
			for(i = 0; i < length; i++)
			{
				value = 0;
				value = Util_charHexToInt(data[2*i]) << 4;
				value |= Util_charHexToInt(data[2*i + 1]);
				*(char*)(addr + i) = value;
			}

			// write-back invalidate the cache
			Util_cacheWbInv(addr, length);

			// reply ok
			sprintf(packet->sendData, OK_RESPONSE);
			packet->sendLength = 2;

			return NO_ERR;
		}
		case STEP_CMD:
		{
			// only allow step if we have stopped at a
			// restartable position in code
			if(!monSession->nonRestartable)
			{
				// clear the CSTOPPED bit
				MFREG0 = MFREG0_CSTOPPED;

				// clear the CSTARTED bit
				MFREG0 = MFREG0_CSTARTED;

				// request the core to step
				MFREG0 = MFREG0_STEP;

				// reset the halted flag and set a pending run
				monSession->isHalted = false;
				monSession->pendingRun = true;

				// we should not send a response
				*sendResponse = false;
			}
			else
			{
				// report a hangup for now
				sprintf(packet->sendData,"S0%d", SIGNAL_HUP);
				packet->sendLength = 3;
			}

			return NO_ERR;
		}
		case CONTINUE_CMD:
		{
			// only allow continue if we have stopped at a
			// restartable position in code
			if(!monSession->nonRestartable)
			{
				// clear the CSTOPPED bit
				MFREG0 = MFREG0_CSTOPPED;

				// request the core to run
				MFREG0 = MFREG0_RUN;

				// reset the halted flag and set a pending run
				monSession->isHalted = false;
				monSession->pendingRun = true;

				// we should not send a response
				*sendResponse = false;
			}
			else
			{
				// report a hangup for now
				sprintf(packet->sendData,"S0%d", SIGNAL_HUP);
				packet->sendLength = 3;
			}

			return NO_ERR;
		}
		case STATUS_CMD:
		{
			if(monSession->isHalted)
			{
				sprintf(packet->sendData,"S0%d", SIGNAL_TRAP);
				packet->sendLength = 3;
			}
			if(monSession->nonRestartable)
			{
				sprintf(packet->sendData,"S0%d", SIGNAL_HUP);
				packet->sendLength = 3;
			}
			return NO_ERR;
		}
		case REMOVE_BREAKPOINT_CMD:
		{
			int error          = NO_ERR;
			int breakType      = 0;
			uint32_t breakAddr = 0;
			error = sscanf(packet->receiveData,"%x,%x,%x", &breakType, &breakAddr);

			if(error == EOF)
			{
				// we will simply return no error and leave
				// the send string empty
				return NO_ERR;
			}

			removeSWBP(monSession, breakAddr);

			// reply ok
			sprintf(packet->sendData, OK_RESPONSE);
			packet->sendLength = 2;
			return NO_ERR;
		}
		case SET_BREAKPOINT_CMD:
		{
			// only allow saetting breakpoints if we have stopped at a
			// restartable position in code
			if(!monSession->nonRestartable)
			{
				int breakType        = 0;
				uint32_t breakAddr   = 0;
				uint32_t breakLength = 0;
				int error            = NO_ERR;

				error = sscanf(packet->receiveData,"%x,%x,%x", &breakType, &breakAddr, &breakLength);

				if(error == EOF)
				{
					// we will simply return no error and leave
					// the send string empty
					return NO_ERR;
				}
				// only support software breakpoints
				if(BREAKPOINT_TYPE_SW  == breakType)
				{
					error = setSWBP(monSession, breakAddr, breakLength);
					if(NO_ERR != error)
					{
						sprintf(packet->sendData, "E%.2x", error);
						packet->sendLength = 3;
					}
					else
					{
						// reply ok
						sprintf(packet->sendData, OK_RESPONSE);
						packet->sendLength = 2;
					}
				}
			}
			else
			{
				sprintf(packet->sendData, "E%c%c", hexCharacters[ERR_HALTED_AT_NONRESTARTABLE & 0xF]
				                           , hexCharacters[(ERR_HALTED_AT_NONRESTARTABLE >> 4) & 0xF]);
				packet->sendLength = 3;
			}

			return NO_ERR;
		}
		case READ_ONE_REGISTER_CMD:
		{
			GDBRegister regNo;
			uint32_t    value = 0;
			int         error = NO_ERR;

			// always update the PC and IRP in the register file
			// we need to do this because we don't know
			// what the value of these is in the AINT ISR
			if(INT_SRC_AINT == interruptSource)
			{
				// Ensure the last bit is masked off
				// it is always assumed to be 0
				monSession->coreRegisterFile[GDB_PC] = ARP & 0xFFFFFFFE;
			}
			else
			{
				// Ensure the last bit is masked off
				// it is always assumed to be 0
				monSession->coreRegisterFile[GDB_PC] = IRP & 0xFFFFFFFE;
			}

			// get the register number
			error = sscanf(packet->receiveData,"%x", &regNo);
			if(error == EOF)
			{
				// we will simply return no error and leave
				// the send string empty
				return NO_ERR;
			}

			// if register is cached, return that value
			if(NUM_CORE_REGISTERS > regNo)
			{
				sprintf(packet->sendData,"%.8x", toTargetOrder(monSession->coreRegisterFile[regNo], ENDIAN));
			}
			else
			{
				// perform the read of the register manually
				readWriteReg(&value, regNo, REG_READ_FLAG);

				error = sprintf(packet->sendData,"%.8x", toTargetOrder(value, ENDIAN));
				// we will always write 8 bytes
				if(error == 8)
				{
					error = NO_ERR;
					packet->sendLength = 8;
				}
				else
				{
					error = ERR_BUFF_WRITE;
				}
			}
			return error;
		}
		case WRITE_ONE_REGISTER_CMD:
		{
			GDBRegister regNo;
			uint32_t value;
			int error;

			// get the register number
			error = sscanf(packet->receiveData,"%x=%x", &regNo, &value);
			if(error == EOF)
			{
				// we will simply return no error and leave
				// the send string empty
				return NO_ERR;
			}

			// swizzle to the correct order
			value = toTargetOrder(value, ENDIAN);

			if(NUM_CORE_REGISTERS > regNo)
			{
				monSession->coreRegisterFile[regNo] = value;
			}
			else
			{
				// perform the read of the register manually
				readWriteReg(&value, regNo, REG_WRITE_FLAG);
			}

			// reply ok
			sprintf(packet->sendData, OK_RESPONSE);
			packet->sendLength = 2;

			return NO_ERR;
		}
		case QUERY_CMD:
		{
			// qSupported[:feature;feature;...]
			if(0 == strncmp("Supported", packet->receiveData, 9))
			{
				sprintf(packet->sendData,"PacketSize=%x", MAX_PACKET_SIZE);
				packet->sendLength = 14;
			}
			// qSymbol::
			else if((packet->receiveLength == 8) && (0 == strncmp("Symbol::", packet->receiveData, 8)))
			{
				sprintf(packet->sendData,"%2x", OK_RESPONSE);
				packet->sendLength = 2;
			}

			// qAttached
			else if((packet->receiveLength == 8) && (0 == strncmp("Attached", packet->receiveData, 8)))
			{
				sprintf(packet->sendData,"1");
				packet->sendLength = 1;
			}

			// all other commands are unimplemented and will return size = 0;
			return NO_ERR;
		}
		case DETACH_CMD:
		case KILL_CMD:
		{
			// clear the CSTOPPED bit
			MFREG0 = MFREG0_CSTOPPED;

			// request the core to run
			MFREG0 = MFREG0_RUN;

			// reset the halted flag and set a pending run
			monSession->isHalted = false;
			monSession->pendingRun = true;

			// remove all breakpoints
			clearAllSWBP(monSession);

			sprintf(packet->sendData, OK_RESPONSE);
			packet->sendLength = 2;

			return NO_ERR;
		}
		case SET_THREAD_CMD:
		{
			sprintf(packet->sendData, OK_RESPONSE);
			packet->sendLength = 2;

			return NO_ERR;
		}
		case HALT_CMD:
		{
			// at this point we should be halted, so just
			// send the signal
			if(!monSession->nonRestartable)
			{
				monSession->isHalted = true;
				sprintf(packet->sendData,"S0%d", SIGNAL_TRAP);
				packet->sendLength = 3;
			}
			else
			{
				monSession->isHalted = true;
				sprintf(packet->sendData,"S0%d", SIGNAL_HUP);
				packet->sendLength = 3;
			}
			return NO_ERR;
		}
		case VCONT_QUERY_CMD:
		{
			if(0 == strncmp("Cont?", packet->receiveData, 6))
			{
				sprintf(packet->sendData,"vCont;c;s");
				packet->sendLength = 9;
			}
			return NO_ERR;
		}
		// not supported
		case WRITE_ALL_REGISTERS_CMD:
		default:
		{
			return NO_ERR;
		}
	}

}

/**
 * FUNCTION NAME: Monitor_getPendingRun
 *
 * DESCRIPTION: Read the pendingRun member of Mon_Session
 *
 * ARGUMENTS:
 *              Mon_Session   *monSession: Pointer to the monitor session
 *
 * RETURN:		pendingRun member of Mon_Session
 *
 */
extern bool Monitor_getPendingRun(struct Mon_Session *monSession)
{
	return monSession->pendingRun;
}

/**
 * FUNCTION NAME: Monitor_resetPendingRun
 *
 * DESCRIPTION: Reset the pending run member of Mon_Session
 *
 * ARGUMENTS:
 *              Mon_Session   *monSession: Pointer to the monitor session
 */
extern void Monitor_resetPendingRun(struct Mon_Session *monSession)
{
	monSession->pendingRun = 0;
}

/**
 * FUNCTION NAME: Monitor_getIsHalted
 *
 * DESCRIPTION: Read the isHalted member of Mon_Session
 *
 * ARGUMENTS:
 *              Mon_Session   *monSession: Pointer to the monitor session
 *
 * RETURN:		isHalted member of Mon_Session
 *
 */
extern bool Monitor_getIsHalted(struct Mon_Session *monSession)
{
	return monSession->isHalted;
}

/**
 * FUNCTION NAME: Monitor_setIsHalted
 *
 * DESCRIPTION: Set the isHalted member of Mon_Session
 *
 * ARGUMENTS:
 *              Mon_Session   *monSession: Pointer to the monitor session
 */
extern void Monitor_setIsHalted(struct Mon_Session *monSession)
{
	monSession->isHalted = true;
}

/**
 * FUNCTION NAME: Monitor_setReasonforHaltBPStep
 *
 * DESCRIPTION: Set the reasonforHalt member of Mon_Session to REASON_HALT_BP_STEP
 *
 * ARGUMENTS:
 *              Mon_Session   *monSession: Pointer to the monitor session
 */
extern void Monitor_setReasonforHaltBPStep(struct Mon_Session *monSession)
{
	monSession->reasonHalt = REASON_HALT_BP_STEP;
}

/**
 * FUNCTION NAME: Monitor_checkNonRestartable
 *
 * DESCRIPTION: Check for the halted in non-interruptible code condition
 *
 * ARGUMENTS: None
 *
 * RETURN:	Halted in non-interruptible code condition
 *
 */
extern bool Monitor_checkNonRestartable(void)
{
	if(DBG_STAT & DBGSTAT_HALTED_IN_NONRESTARTABLE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * FUNCTION NAME: toTargetOrder
 *
 * DESCRIPTION: Function for swizzling the bytes to target order.
 *
 * ARGUMENTS:
 * 				uint32_t value: Value to be swizzled
 * 				int     endian: The endianness to swizzle the value to
 *
 * RETURN: A swizzled 32-bit value
 */
static uint32_t toTargetOrder(uint32_t value, int endian)
{
	uint32_t retValue = 0;
	if(LITTLE_ENDIAN == endian)
	{
		retValue =  (value >> 24) & 0xFF;
		retValue |=  (value >> 8) & 0xFF00;
		retValue |=  (value << 8) & 0xFF0000;
		retValue |=  (value << 24) & 0xFF000000;
	}
	return retValue;
}

/**
 * FUNCTION NAME: readWriteReg
 *
 * DESCRIPTION: Function for reading/writing non-cached DSP registers.
 *
 * ARGUMENTS:
 * 				uint32_t   *value: Pointer to the value to be read/written
 * 				GDBRegister regNo: The register to read/write
 * 				int     readWrite: The read/write flag
 *
 * RETURN:		Error code
 *
 */
static int readWriteReg(uint32_t *value, GDBRegister regNo, int readWrite)
{
	int error = NO_ERR;
	*value = 0;
	switch(regNo)
	{
	    case GDB_IRP:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = IRP;
			}
			else
			{
				IRP = *value;
			}
			break;
		}
	    case GDB_IFR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = IFR;
			}
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
	    case GDB_NRP:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = NRP;
			}
			else
			{
				NRP = *value;
			}
			break;
		}
		case GDB_AMR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = AMR;
			}
			else
			{
				AMR = *value;
			}
			break;
		}
		case GDB_ISR:
		{
			// write only register
			if(readWrite == REG_READ_FLAG)
			{
				error = ERR_REG_READ_WRITE;
			}
			else
			{
				ISR = *value;
			}
			break;
		}
		case GDB_ICR:
		{
			// write only register
			if(readWrite == REG_READ_FLAG)
			{
				error = ERR_REG_READ_WRITE;
			}
			else
			{
				ICR = *value;
			}
			break;
		}
		case GDB_IER:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = IER;
			}
			else
			{
				IER = *value;
			}
			break;
		}
		case GDB_ISTP:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = ISTP;
			}
			else
			{
				ISTP = *value;
			}
			break;
		}
		case GDB_FADCR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = FADCR;
			}
			else
			{
				FADCR = *value;
			}
			break;
		}
		case GDB_FAUCR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = FAUCR;
			}
			else
			{
				FAUCR = *value;
			}
			break;
		}
		case GDB_FMCR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = FMCR;
			}
			else
			{
				FMCR = *value;
			}
			break;
		}
		case GDB_GFPGFR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = GFPGFR;
			}
			else
			{
				GFPGFR = *value;
			}
			break;
		}
		case GDB_DIER:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = DIER;
			}
			else
			{
				DIER = *value;
			}
			break;
		}
		case GDB_REP:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = REP;
			}
			else
			{
				REP = *value;
			}
			break;
		}
		case GDB_TSCL:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = TSCL;
			}
			else
			{
				TSCL = *value;
			}
			break;
		}
		case GDB_TSCH:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = TSCH;
			}
			else
			{
				TSCH = *value;
			}
			break;
		}
		case GDB_ARP:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = ARP;
			}
			// we will not allow writes to this register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_ILC:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = ILC;
			}
			else
			{
				ILC = *value;
			}
			break;
		}
		case GDB_RILC:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = RILC;
			}
			else
			{
				RILC = *value;
			}
			break;
		}
		case GDB_DNUM:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = gdb_get_dsp_id();
			}
			// this is a read only register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_SSR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = SSR;
			}
			else
			{
				SSR = *value;
			}
			break;
		}
		case GDB_GPLYA:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = GPLYA;
			}
			else
			{
				GPLYB = *value;
			}
			break;
		}
		case GDB_GPLYB:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = GPLYB;
			}
			else
			{
				GPLYB = *value;
			}
			break;
		}
		case GDB_TSR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = TSR;
			}
			else
			{
				TSR = *value;
			}
			break;
		}
		case GDB_ITSR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = ITSR;
			}
			else
			{
				ITSR = *value;
			}
			break;
		}
		case GDB_NTSR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = NTSR;
			}
			else
			{
				NTSR = *value;
			}
			break;
		}
		case GDB_EFR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = EFR;
			}
			// this is a read only register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_IERR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = IERR;
			}
			else
			{
				IERR = *value;
			}
			break;
		}
		case GDB_DMSG:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = DMSG;
			}
			else
			{
				DMSG = *value;
			}
			break;
		}
		case GDB_CMSG:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = CMSG;
			}
			else
			{
				CMSG = *value;
			}
			break;
		}
		case GDB_DT_DMA_ADDR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = DT_DMA_ADDR;
			}
			else
			{
				DT_DMA_ADDR = *value;
			}
			break;
		}
		case GDB_DT_DMA_DATA:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = DT_DMA_DATA;
			}
			else
			{
				DT_DMA_DATA = *value;
			}
			break;
		}
		case GDB_DT_DMA_CNTL:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = DT_DMA_CNTL;
			}
			else
			{
				DT_DMA_CNTL = *value;
			}
			break;
		}
		case GDB_TCU_CNTL:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = TCU_CNTL;
			}
			else
			{
				TCU_CNTL = *value;
			}
			break;
		}
		case GDB_RTDX_REC_CNTL:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = RTDX_REC_CNTL;
			}
			else
			{
				RTDX_REC_CNTL = *value;
			}
			break;
		}
		case GDB_RTDX_XMT_CNTL:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = RTDX_XMT_CNTL;
			}
			else
			{
				RTDX_XMT_CNTL = *value;
			}
			break;
		}
		case GDB_RTDX_CFG:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = RTDX_CFG;
			}
			else
			{
				RTDX_CFG = *value;
			}
			break;
		}
		case GDB_RTDX_RDATA:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = RTDX_RDATA;
			}
			else
			{
				RTDX_RADDR = *value;
			}
			break;
		}
		case GDB_RTDX_WDATA:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = RTDX_WDATA;
			}
			else
			{
				RTDX_WDATA = *value;
			}
			break;
		}
		case GDB_RTDX_RADDR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = RTDX_RADDR;
			}
			else
			{
				RTDX_RADDR = *value;
			}
			break;
		}
		case GDB_RTDX_WADDR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = RTDX_WADDR;
			}
			else
			{
				RTDX_WADDR = *value;
			}
			break;
		}
		case GDB_MFREG0:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = MFREG0;
			}
			// we will not allow writes to this register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_DBG_STAT:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = DBG_STAT;
			}
			// read only register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_BRK_EN:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = BRK_EN;
			}
			else
			{
				BRK_EN = *value;
			}
			break;
		}
		case GDB_HWBP0_CNT:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = HWBP0_CNT;
			}
			// we will not allow writes to this register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_HWBP0:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = HWBP0;
			}
			// we will not allow writes to this register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_HWBP1:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = HWBP1;
			}
			// we will not allow writes to this register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_HWBP2:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = HWBP2;
			}
			// we will not allow writes to this register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_HWBP3:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = HWBP3;
			}
			// we will not allow writes to this register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_OVERLAY:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = OVERLAY;
			}
			else
			{
				OVERLAY = *value;
			}
			break;
		}
		case GDB_PC_PROF:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = PC_PROF;
			}
			// read only register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_ATSR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = ATSR;
			}
			else
			{
				ATSR = *value;
			}
			break;
		}
		case GDB_TRR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = TRR;
			}
			else
			{
				TRR = *value;
			}
			break;
		}
		case GDB_TCRR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = TCRR;
			}
			else
			{
				TCRR = *value;
			}
			break;
		}
		case GDB_DESR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = DESR;
			}
			//read only register
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
		case GDB_DETR:
		{
			if(readWrite == REG_READ_FLAG)
			{
				*value = DETR;
			}
			else
			{
				DETR = *value;
			}
			break;
		}
		default:
		{
			if(readWrite == REG_READ_FLAG)
			{
				error = ERR_REG_READ_WRITE;
			}
			else
			{
				error = ERR_REG_READ_WRITE;
			}
			break;
		}
	}
	return error;
}

