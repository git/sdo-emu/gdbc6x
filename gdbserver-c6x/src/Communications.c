/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Communications.h"

#define STARTING_CHAR          '$'             /**< The starting character for an RSP packet   */
#define CHECKSUM_CHAR          '#'             /**< The checksum character for an RSP packet   */
#define ESC_CHAR1              '*'             /**< This character must be escaped             */
#define ESC_CHAR2              '}'             /**< This character must be escaped             */
#define ACK                    '+'             /**< The acknowledge character                  */
#define NACK                   '-'             /**< The not-acknowledge character              */
#define STATUS_ARM_READY_FLAG  0x000000001     /**< Signal the ARM that a message is available */
#define STATUS_COUNT_SHIFT     8               /**< The message length will be encoded here    */
#define MAX_CHAR_COUNT         40              /**< The maximum amount of characters to check
                                                    before giving up on communication          */

/**
 * This function is used to decode the RSP command type
 */
static void decodeCmdId(char cmd, GDBCommandId *id);

/**
 * This function is used increment the readByte by handling all
 * escape characters
 */
static int incrementChar(char *readByte);

/**
 * FUNCTION NAME: Comm_initialize
 *
 * DESCRIPTION: This function is used to initialize the debug session with
 *              the ARM core
 *
 * ARGUMENTS:
 * 				Comm_Session *commSession: Handle to a commSession with
 * 				                           initialized fields
 *
 * RETURN:		Error code
 *
 */
int Comm_initialize(struct Comm_Session *commSession)
{
	uint32_t readVal = 0;

	if(0 == commSession->isInitialized)
	{
		// first we will check if this is the DSP that we are
		// intending to communicate with
		Util_readAddr(commSession->sharedAddress, &readVal);
		if(readVal != gdb_get_dsp_id() + 1)
		{
			return ERR_COMM_WRONG_DSP;
		}
		else
		{
			// if the correct DSP is requested we will now write
			// the response
			commSession->isInitialized = true;
			Util_writeAddr(commSession->sharedAddress, 0xffffffff);
			return NO_ERR;
		}
	}
	else
	{
		return ERR_COMM_INIT_FAIL;
	}
}

/**
 * FUNCTION NAME: Comm_getPacket
 *
 * DESCRIPTION: This function is used to receive an RSP formatted packet
 *
 * ARGUMENTS:
 * 				Comm_Session *commSession: Handle to the session
 * 				Packet            *packet: Returns the data read from the RSP packet
 * 				int         maxPacketSize: The maximum packet size supported
 *
 * RETURN:		Error code
 *
 */
extern int Comm_getPacket(struct Comm_Session *commSession, struct Packet *packet, int maxPacketSize)
{
	char    *readByte  = 0;
	int      charCount = 0;
	int      incCount  = 0;
	int      error     = 0;
	bool     foundHalt = false;

	packet->receiveLength = 0;

	// TODO: argument error checking
	if(commSession->isInitialized == false)
	{
		return ERR_INIT_COMM;
	}

	// ensure we perform cache operations
	Util_cacheInv(commSession->sharedAddressComm, maxPacketSize);

	// create an iterator for the word
	readByte = (char*)commSession->sharedAddressComm;

	// first check for halt character 0x03
	if(*readByte == 0x03)
	{
		foundHalt = true;
		// just set the command has halt
		packet->commandId = HALT_CMD;
	}

	if(!foundHalt)
	{
		// find the starting character
		while((*readByte != STARTING_CHAR) && (charCount < MAX_CHAR_COUNT) && (*readByte != ACK))
		{
			readByte++;
			charCount++;
		}

		if(*readByte == ACK)
		{
			return NO_ERR;
		}

		if(charCount >= MAX_CHAR_COUNT)
		{
			return ERR_STARTING_CHAR;
		}

		// second character will always be the command
		incCount = incrementChar(readByte);

		readByte += incCount;

		// decode the type of command
		decodeCmdId(*readByte, &(packet->commandId));

		// move pointer to starting character of data
		incCount = incrementChar(readByte);

		readByte += incCount;

		// set the buffer to the beginning of the data
		packet->receiveData = readByte;

		// we don't care about the checksum since we
		// are writing to memory and expect no corruption
		while(*readByte != CHECKSUM_CHAR)
		{
			// move the pointer
			incCount = incrementChar(readByte);

			readByte += incCount;

			// move the array index
			packet->receiveLength++;
		}

		// insert the terminating character
		packet->receiveData[packet->receiveLength] = '\0';
	}

	return error;

}

/**
 * FUNCTION NAME: Comm_sendPacket
 *
 * DESCRIPTION: This function is used to send an RSP formatted packet
 *
 * ARGUMENTS:
 * 				Comm_Session *commSession: Handle to the session
 * 				Packet            *packet: Returns the data read from the RSP packet
 * 				bool             response: Is a response necessary
 *
 * RETURN:		Error code
 *
 */
extern int Comm_sendPacket(struct Comm_Session *commSession, struct Packet *packet, bool response)
{
	unsigned int checkSum = 0;
	int   i               = 0;

	// TODO: argument error checking
	if(commSession->isInitialized == false)
	{
		return ERR_INIT_COMM;
	}

	if(response)
	{
		// set the write pointer
		packet->sendData = (char*)commSession->sharedAddressComm;

		// send an empty packet
		if(packet->sendLength == 0)
		{
			packet->sendData[0] = STARTING_CHAR;
			packet->sendData[1] = CHECKSUM_CHAR;
			packet->sendData[2] = '0';
			packet->sendData[3] = '0';
		}
		else
		{
			// write the starting character
			packet->sendData[0] = STARTING_CHAR;

			// calculate the checksum
			for(i = 0; i < packet->sendLength; i++)
			{
				checkSum += packet->sendData[i + 1];
			}

			// checksum is only least significant byte
			checkSum = checkSum & 0xff;

			// write checksum
			packet->sendData[packet->sendLength + 1] = CHECKSUM_CHAR;
			if(checkSum < 16)
			{
				packet->sendData[packet->sendLength + 2] = '0';
				packet->sendData[packet->sendLength + 3] = hexCharacters[checkSum & 0xf];
			}
			else
			{
				packet->sendData[packet->sendLength + 2] = hexCharacters[checkSum >> 4];
				packet->sendData[packet->sendLength + 3] = hexCharacters[checkSum & 0xf];
			}
		}

          // ensure everything gets written back to memory
		Util_cacheWbInv(commSession->readyAddr + 4, packet->sendLength);

		// write back ready bit and size
		Util_writeAddr(commSession->readyAddr, ((packet->sendLength + 4 << STATUS_COUNT_SHIFT)
				                                       | STATUS_ARM_READY_FLAG));
	}
	else
	{
		// no response is needed, just clear the ready bit
		Util_writeAddr(commSession->readyAddr, 0);
	}

	return NO_ERR;
}

/**
 * FUNCTION NAME: Comm_sendStatus
 *
 * DESCRIPTION: Send a status packet for the kernel module
 *
 * ARGUMENTS:
 * 				Comm_Session *commSession: Handle to the session
 * 				uint32_t           packet: Packet to send
 *
 */
extern void Comm_sendStatus(struct Comm_Session *commSession, uint32_t packet)
{
	// write back ready bit and Packet->sendLength
	Util_writeAddr(commSession->readyAddr, (packet | STATUS_ARM_READY_FLAG));

	// ensure everything gets written back to memory
	Util_cacheWbInv(commSession->readyAddr, 4);

}

/**
 * FUNCTION NAME: Comm_setCommInterrupt
 *
 * DESCRIPTION: Set the communications interrupt
 *
 * ARGUMENTS:
 * 				Comm_Session *commSession: Handle to the session
 *
 */
extern void Comm_setCommInterrupt(struct Comm_Session *commSession)
{
	ISR = (0x1 << commSession->intNum);
}

/**
 * FUNCTION NAME: decodeCmdId
 *
 * DESCRIPTION: This function is used to decode the RSP command type
 *
 * ARGUMENTS:
 * 				char          cmd: The command
 * 				GDBCommandId  *id: The command id to be filled
 *
 */
static void decodeCmdId(char cmd, GDBCommandId *id)
{
	switch(cmd)
	{
		case 'g':
			*id = READ_ALL_REGISTERS_CMD;
			break;
		case 'G':
			*id = WRITE_ALL_REGISTERS_CMD;
			break;
		case 'm':
			*id = READ_MEMORY_CMD;
			break;
		case 'M':
			*id = WRITE_MEMORY_CMD;
			break;
		case 's':
			*id = STEP_CMD;
			break;
		case 'c':
			*id = CONTINUE_CMD;
			break;
		case '?':
			*id = STATUS_CMD;
			break;
		case 'z':
			*id = REMOVE_BREAKPOINT_CMD;
			break;
		case 'Z':
			*id = SET_BREAKPOINT_CMD;
			break;
		case 'p':
			*id = READ_ONE_REGISTER_CMD;
			break;
		case 'P':
			*id = WRITE_ONE_REGISTER_CMD;
			break;
		case 'q':
			*id = QUERY_CMD;
			break;
		case 'D':
			*id = DETACH_CMD;
			break;
		case 'H':
			*id = SET_THREAD_CMD;
			break;
		case 'v':
			*id = VCONT_QUERY_CMD;
			break;
		case 0x03:
			*id = HALT_CMD;
			break;
		case 'k':
			*id = KILL_CMD;
			break;
		default:
			*id = UNKNOWN_CMD;
			break;
	}

	return;
}

/**
 * FUNCTION NAME: incrementChar
 *
 * DESCRIPTION: This function is used increment the readByte by handling all
 *              escape characters
 *
 * ARGUMENTS:
 * 				char cmd: The current byte we are pointed to
 *
 * RETURN:		The number of bytes the pointer must be incremented by
 *
 */
static int incrementChar(char *readByte)
{
	// we will always at least increment by 1
	int count = 1;

	// increment the pointer to next character
	readByte++;

	// increment the pointer until we reach a non-escapable character
	while((*readByte == ESC_CHAR1) || (*readByte == ESC_CHAR2))
	{
		readByte++;
		count++;
	}

	return count;
}
