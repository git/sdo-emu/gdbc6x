/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Server.h"
#include "GDB_server.h"

#define EXPORT __attribute__((visibility("protected")))
#define GDB_SINGLE_SHARED_BUF_SIZE ((MAX_PACKET_SIZE)/4 + 2)

#ifdef KEYSTONE_PLATFORM
#define MAX_NUM_CORES (8)
#endif

#ifdef DRA7xx_PLATFORM
#define MAX_NUM_CORES (2)
#endif

/**
 * Data to be managed globally. This data is indexed by gdb_get_dsp_id() and kept in a
 * shared location.
 */
struct GDB_globalData
{
	/**
	 *  Shared buffer format {Initialization(4 bytes),Status (4 bytes), Data (MAX_PACKET_SIZE)						 
	 */
	uint32_t gdb_sharedBuffer[GDB_SINGLE_SHARED_BUF_SIZE * MAX_NUM_CORES];
	struct Comm_Session commSession[MAX_NUM_CORES];
	struct Mon_Session monSession[MAX_NUM_CORES];
};

#pragma DATA_SECTION(gdb_globalData, ".gdb_server")
#pragma DATA_ALIGN(gdb_globalData, 8192)
struct GDB_globalData gdb_globalData;

/**
 * This function is used for claiming ownership of
 * the execution control resources
 */
static void claimMfreg0(void);

/**
 * This function performs global initialization of the debug server
 */
static int initGlob(int intNum);

/**
 * This function wires up the interrupt using DSPBIOS hooks
 */
static void setInterrupt(int eventId);

/**
 * FUNCTION NAME: GDB_init
 *
 * DESCRIPTION: Initializes the debug server with user allocated resources
 *
 * ARGUMENTS:
 * 			    int intNum: 			   The interrupt number that the server
 * 							               will use for communication
 *
 * RETURN:		Error code
 *
 */
extern int GDB_server_init(int intNum)
{
	int error = NO_ERR;
	unsigned int core_id;

	//get current core id
	core_id = gdb_get_dsp_id();

	gdb_globalData.commSession[core_id].intNum        = intNum;
	gdb_globalData.commSession[core_id].isInitialized = false;
	gdb_globalData.commSession[core_id].sharedAddress = (uint32_t)&gdb_globalData.gdb_sharedBuffer
													[GDB_INIT_IDX + (core_id * GDB_SINGLE_SHARED_BUF_SIZE)];
	gdb_globalData.commSession[core_id].readyAddr     = (uint32_t)&gdb_globalData.gdb_sharedBuffer
													[GDB_STATUS_IDX + (core_id * GDB_SINGLE_SHARED_BUF_SIZE)];
	gdb_globalData.commSession[core_id].sharedAddressComm = (uint32_t)&gdb_globalData.gdb_sharedBuffer
													[GDB_DATA_IDX + (core_id * GDB_SINGLE_SHARED_BUF_SIZE)];

	// write back all communication session data
	Util_cacheWbInv((uint32_t)&gdb_globalData.commSession, sizeof(struct Comm_Session) * MAX_NUM_CORES);

	// initialize the monitor data structure
	Monitor_initialize(&gdb_globalData.monSession[core_id]);

	// initialize ARM - C66x DSP interprocessor communication
    	error = Util_initArmDspIpc();

	setInterrupt(AET_RTOS_INT);

	return error;
}

/**
 * FUNCTION NAME: GDB_initGlob
 *
 * DESCRIPTION: Performs global initialization of the debug server
 *
 * ARGUMENTS:
 * 				int intNum: 			   The interrupt number that the server
 * 							               will use for communication
 *
 * RETURN:		Error code
 *
 */
extern int GDB_server_initGlob(int intNum)
{
	return (initGlob(intNum));
}

/**
 * FUNCTION NAME: GDB_initLocal
 *
 * DESCRIPTION: Performs local initialization of the debug server
 *
 * ARGUMENTS: None
 *
 *
 * RETURN:		Error code
 *
 */
extern int GDB_server_initLocal(void)
{
	int error = NO_ERR;
	int i = 0;

    Util_cacheInv((uint32_t)&gdb_globalData,sizeof(gdb_globalData));

	// check if global initialization has been performed
	if(gdb_globalData.commSession[0].intNum == 0)
	{
		return ERR_GLOB_NOT_CALLED;
	}

	for(i = 0; i < MAX_NUM_CORES; i++)
	{
		gdb_globalData.commSession[i].intNum        = gdb_globalData.commSession[0].intNum;
		gdb_globalData.commSession[i].isInitialized = false;
		gdb_globalData.commSession[i].sharedAddress = (uint32_t)&gdb_globalData.gdb_sharedBuffer
														[GDB_INIT_IDX + (i * GDB_SINGLE_SHARED_BUF_SIZE)];
		gdb_globalData.commSession[i].readyAddr     = (uint32_t)&gdb_globalData.gdb_sharedBuffer
														[GDB_STATUS_IDX + (i * GDB_SINGLE_SHARED_BUF_SIZE)];
		gdb_globalData.commSession[i].sharedAddressComm = (uint32_t)&gdb_globalData.gdb_sharedBuffer
														[GDB_DATA_IDX + (i * GDB_SINGLE_SHARED_BUF_SIZE)];
	}

	// write back all communication session data
	Util_cacheWbInv((uint32_t)&gdb_globalData.commSession, sizeof(struct Comm_Session) * 8);

	// initialize the monitor data structure
	Monitor_initialize(&gdb_globalData.monSession[gdb_get_dsp_id()]);

	// initialize ARM - C66x DSP interprocessor communication
    	error = Util_initArmDspIpc();

	setInterrupt(AET_RTOS_INT);

	return error;
}

/**
 * FUNCTION NAME: initGlob
 *
 * DESCRIPTION: This function performs global initialization of the debug server
 *
 * ARGUMENTS:
 * 				int intNum: 			   The interrupt number that the server
 * 							               will use for communication
 *
 * RETURN:		Error code
 *
 */
static int initGlob(int intNum)
{
	// range check the arguments
	if((INT_HIGH < intNum) || (INT_LOW > intNum))
	{
		return ERR_INT_OUT_RANGE;
	}

	// initialize the communication data structure
	gdb_globalData.commSession[0].intNum        = intNum;
	gdb_globalData.commSession[0].isInitialized = false;
	gdb_globalData.commSession[0].sharedAddress = (uint32_t)&gdb_globalData.gdb_sharedBuffer
													[GDB_INIT_IDX];
	gdb_globalData.commSession[0].readyAddr     = (uint32_t)&gdb_globalData.gdb_sharedBuffer
													[GDB_STATUS_IDX];
	gdb_globalData.commSession[0].sharedAddressComm = (uint32_t)&gdb_globalData.gdb_sharedBuffer
													[GDB_DATA_IDX];

	// write back all communication session data
	Util_cacheWbInv((uint32_t)&gdb_globalData.commSession, sizeof(struct Comm_Session) * MAX_NUM_CORES);

        return NO_ERR;
}

/**
 * FUNCTION NAME: setInterrupt
 *
 * DESCRIPTION: This function wires up the interrupt using DSPBIOS hooks
 *
 * ARGUMENTS:
 * 				eventId: The event ID that must trigger the interrupt
 */
static void setInterrupt(int eventId)
{
	uint32_t csr = CSR;
	uint32_t dier = DIER;

	// hook up the ISR
	Hwi_plug(gdb_globalData.commSession[gdb_get_dsp_id()].intNum, (Hwi_PlugFuncPtr)dispatchISR);

	// map the AET event to the interrupt
	Hwi_eventMap(gdb_globalData.commSession[gdb_get_dsp_id()].intNum, eventId);

	// set given interrupt as a real time (continues to be
	// serviced while core is halted)
	DIER = dier | (0x1 << gdb_globalData.commSession[gdb_get_dsp_id()].intNum);

	// enable the communications interrupt
	Hwi_enableInterrupt(gdb_globalData.commSession[gdb_get_dsp_id()].intNum);

	// plug in the analysis interrupt
	Hwi_plug(AINT_NUM, (Hwi_PlugFuncPtr)debug_Preamble);
	Hwi_enableInterrupt(AINT_NUM);

	//enable interrupts
	CSR = csr | 0x1;
}

/**
 * FUNCTION NAME: handleComm
 *
 * DESCRIPTION: This function is used for communication to the ARM
 *
 * ARGUMENTS:	None
 */
extern void handleComm(void)
{
	uint32_t detach = 0;
	bool  response  = false;
	int      error  = NO_ERR;
	uint32_t mfreg0 = 0;
        struct Packet packet;
	packet.sendData = (char*)gdb_globalData.commSession[gdb_get_dsp_id()].sharedAddressComm;

	// initialize the communication session
	if(true != gdb_globalData.commSession[gdb_get_dsp_id()].isInitialized)
	{
		if(Comm_initialize(&gdb_globalData.commSession[gdb_get_dsp_id()]) == NO_ERR)
		{
			claimMfreg0();

			// check MFREG0 to ensure we were able to claim it
			mfreg0 = MFREG0;
			if((mfreg0 & MFREG0_CLAIMED) != MFREG0_CLAIMED)
			{
				error = ERR_MFREG0_NOT_CLAIMED;
			}

			// issue a run command
			// up to this point, the core should be running
			// a functional run. Halting is not enabled in
			// this mode, so we should change the mode to a
			// (debug) run
			if(NO_ERR == error)
			{
				MFREG0 = MFREG0_RUN;
			}
		}
	}
	else
	{
		// find the source of the halt
		if(gdb_globalData.monSession[gdb_get_dsp_id()].reasonHalt == REASON_HALT_BP_STEP)
		{
			// reset reason for halt
			gdb_globalData.monSession[gdb_get_dsp_id()].reasonHalt = 0;

			// for breakpoints and step we will set the command to halt
			// in order to service it properly - this will notify GDB
			// that there has been a halt
			packet.commandId = HALT_CMD;

			error = Monitor_processPacket(&gdb_globalData.monSession[gdb_get_dsp_id()], &packet, MAX_PACKET_SIZE, &response, INT_SRC_NOT_AINT);
			if(NO_ERR == error)
			{
				Comm_sendPacket(&gdb_globalData.commSession[gdb_get_dsp_id()], &packet, response);
			}
		}
		else
		{
			// see if we have received a detach command and interrupt

			// is actually meant for us
			Util_readAddr(gdb_globalData.commSession[gdb_get_dsp_id()].readyAddr, &detach);
			if(detach & STATUS_NEW_PACKET)
			{
				if(DETACH_COMM != detach)
				{
					if(!gdb_globalData.monSession[gdb_get_dsp_id()].isHalted)
					{
						// we must ensure we are halted
						gdb_globalData.monSession[gdb_get_dsp_id()].isHalted = true;
						MFREG0 = MFREG0_HALT;
					}

					// handle the packet
					error = Comm_getPacket(&gdb_globalData.commSession[gdb_get_dsp_id()], &packet, MAX_PACKET_SIZE);
					if(NO_ERR == error)
					{
						error = Monitor_processPacket(&gdb_globalData.monSession[gdb_get_dsp_id()], &packet, MAX_PACKET_SIZE, &response, INT_SRC_NOT_AINT);
					}
					if(NO_ERR == error)
					{
						Comm_sendPacket(&gdb_globalData.commSession[gdb_get_dsp_id()], &packet, response);    
					}

				}
				else
				{
					// core should already be running at this point
					// will de-initialize the communications structure
					gdb_globalData.commSession[gdb_get_dsp_id()].isInitialized = false;

					// remove claim of MFREG0
					MFREG0 = MFREG0_AVAIL;
				}
			}
		}
	}

	// clear AET RTOS interrupt
	Util_clearCtiInt();
}

/**

 * FUNCTION NAME: nonRestartable
 *
 * DESCRIPTION: This function is used for handling the non-Restartable condition.
 *              It is called from the debug preamble
 *
 * ARGUMENTS:	None
 */
extern void nonRestartable(void)
{
	int          error = NO_ERR;
	bool      response = false;
	uint32_t newPacket = 0;
     	struct Packet packet;

	// set the non-restartable flag to true
	gdb_globalData.monSession[gdb_get_dsp_id()].nonRestartable = true;

	// signal that we are at a non-restartable position in code
	// at this point we will just sent a hang up signal
	packet.commandId = HALT_CMD;
	error = Monitor_processPacket(&gdb_globalData.monSession[gdb_get_dsp_id()], &packet, MAX_PACKET_SIZE, &response, INT_SRC_NOT_AINT);
	if(NO_ERR == error)
	{
		Comm_sendPacket(&gdb_globalData.commSession[gdb_get_dsp_id()], &packet, response);
	}

	// continue servicing RSP packets
	while(1)
	{
		// poll for a new packet
		// setting breakpoints will fail and run commands
		// (step and continue) will immediately return
		// the SIGNAL_HUP (hangup)
		Util_readAddr(gdb_globalData.commSession[gdb_get_dsp_id()].readyAddr, &newPacket);
		if(newPacket & STATUS_NEW_PACKET)
		{
			error = Comm_getPacket(&gdb_globalData.commSession[gdb_get_dsp_id()], &packet, MAX_PACKET_SIZE);
			if(NO_ERR == error)
			{
				error = Monitor_processPacket(&gdb_globalData.monSession[gdb_get_dsp_id()], &packet, MAX_PACKET_SIZE, &response, INT_SRC_AINT);
			}
			if(NO_ERR == error)
			{
				Comm_sendPacket(&gdb_globalData.commSession[gdb_get_dsp_id()], &packet, response);
			}

			// clear the AET RTOS interrupt
			// not necessary since at this point we are polling
			Util_clearCtiInt();
		}
	}
}

/**
 * FUNCTION NAME: Load_commSession
 *
 * DESCRIPTION: Loads the monitor session of the specific DSP
 *
 */
extern struct Comm_Session* Load_commSession(void)
{
	return &gdb_globalData.commSession[gdb_get_dsp_id()];
}

/**
 * FUNCTION NAME: Load_monSession
 *
 * DESCRIPTION: Loads the monitor session of the specific DSP
 *
 */
extern struct Mon_Session* Load_monSession(void)
{
	return &gdb_globalData.monSession[gdb_get_dsp_id()];
}

/**
 * FUNCTION NAME: claimMfreg0
 *
 * DESCRIPTION: This function is used for claiming ownership of
 *              the execution control resources
 *
 * ARGUMENTS:	None
 */
static void claimMfreg0(void)
{
	// claim ownership of execution control
	MFREG0 = MFREG0_CLAIM;

	// enable usage of MFREG0
	MFREG0 = MFREG0_ENABLE;

	// enable embedded mode (monitor)
	// this allows the AINT to run on
	// a halt
	MFREG0 = MFREG0_EM_ENABLE;

}




