/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef REGISTER_H_
#define REGISTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef DRA7xx_PLATFORM

/* Macros for DebugSS PLLCTRL registers */
#define DPLL_DEBUG_BASE_ADDR         (0x54171000)
#define PLLCTRL_STATUS_REG           (0x804)
#define PLLCTRL_GO_REG               (0x808)
#define PLLCTRL_CONFIG1_REG          (0x80C)
#define PLLCTRL_CONFIG2_REG          (0x810)
#define PLLCTRL_CONFIG3_REG          (0x814)

//L3 INSTR clock registers
#define CM_L3INSTR_L3_3_BASE_ADDR    (0x4A008000)
#define CM_L3INSTR_L3_3_CLKCTRL      (0xE20)
#define CM_L3INSTR_L3_INSTR_CLKCTRL  (0xE28)

//EMU PRCM registers
#define EMU_CM_BASE_ADDR             (0x4AE07000)
#define CM_EMU_CLKSTCTRL             (0xA00)

#endif

#ifdef KEYSTONE_PLATFORM

#define PSC_BASE                     (0x02350000)
#define PSC_PTCMD                    (PSC_BASE + 0x120)
#define PSC_PTSTAT                   (PSC_BASE + 0x128)
#define PSC_DEBUGSS_PD               (1)
#define PSC_DEBUGSS_MD               (5)
#define PSC_DEBUGSS_MDCTL            (PSC_BASE + 0xA00 + (4 * PSC_DEBUGSS_MD))
#define PSC_DEBUGSS_MDSTAT           (PSC_BASE + 0x800 + (4 * PSC_DEBUGSS_MD))
#define PSC_DEBUGSS_PDCTL            (PSC_BASE + 0x300 + (4 * PSC_DEBUGSS_PD))

#define PSC_ENABLE                   (0x3)

#endif

/**
 * MFREG0 Control Register Values
 */
#define MFREG0_CSTOPPED    (0x1 << 17)                      			/**< CSTOPPED bit                                 */
#define MFREG0_CSTARTED    (0x1 << 16)                      			/**< CSTOPPED bit                                 */
#define MFREG0_FINTB       (0x1 << 14)                      			/**< Halt only on interruptible boundary          */
#define MFREG0_AVAIL       0x00000004                       			/**< Release MFREG0 ownership for the application */
#define MFREG0_CLAIM       0x00000005                       			/**< Claim MFREG0 ownership for the application   */
#define MFREG0_ENABLE      0x00000006                       			/**< Enable MFREG0 ownership for the application  */
#define MFREG0_EM_ENABLE   0x00180000                       			/**< Enable Embedded Mode (monitor) for DSP       */
#define MFREG0_LD_EXE      (0x1 << 15)                      			/**< Enable load of Execution segment             */
#define MFREG0_CLAIMED     0x80000002                       			/**< MFREG0 has been claimed properly             */
#define MFREG0_HALT        (MFREG0_LD_EXE | MFREG0_FINTB)   			/**< Halt command                                 */
#define MFREG0_STEP        (MFREG0_LD_EXE | 0x00000800 | MFREG0_FINTB)  /**< Step command                                 */
#define MFREG0_RUN1        (MFREG0_LD_EXE | 0x00001000)     			/**< Run1 command                                 */
#define MFREG0_RUN         (MFREG0_LD_EXE | 0x00001800)     			/**< Run command                                  */
#define MFREG0_FRUN        (MFREG0_LD_EXE | 0x00002000)     			/**< Functional Run command                       */
#define MFREG0_STEPU       (MFREG0_LD_EXE | 0x00002800)     			/**< Step unconditional command                   */
#define MFREG0_RUN1U       (MFREG0_LD_EXE | 0x00003000)     			/**< Run1 unconditional command                   */
#define MFREG0_RUNU        (MFREG0_LD_EXE | 0x00003800)     			/**< Run unconditional command                    */
#define ENABLE_AET_BRK     0x00000002                       			/**< Enable AET breakpoints                       */

/**
 * DBGSTAT Status Register Values
 */
#define DBGSTAT_HALTED_IN_NONRESTARTABLE 0x00001000  /**< Status - Halted in non-restartable code */
#define DBGSTAT_HALTED_IN_SPLOOP         0x00020000  /**< Status - Halted in an SPLOOP            */

#ifdef __cplusplus
}
#endif /* extern "C" */


#endif /* REGISTER_H_ */
