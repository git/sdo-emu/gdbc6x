/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Util.h"
#include "aet.h"

#define DSP_SYS_HWINFO  (0x01D00004)

/**
 * FUNCTION NAME: gdb_get_dsp_id
 *
 * DESCRIPTION: get dsp id
 *
 * RETURN:		dsp id
 *
 */
extern uint32_t gdb_get_dsp_id(void)
{
#ifdef DRA7xx_PLATFORM

    uint32_t id = (*((uint32_t *)DSP_SYS_HWINFO)) & 0xf;

#endif

#ifdef KEYSTONE_PLATFORM

    uint32_t id = DNUM;

#endif

    return id;
}

/**
 * FUNCTION NAME: Util_charHexToInt
 *
 * DESCRIPTION: Convert a hex character to int
 *
 * ARGUMENTS:
 * 				The character in hex
 *
 * RETURN:		The character converted to an integer
 *
 */
extern int Util_charHexToInt(char hex)
{
	switch(hex)
	{
		case '0':
			return 0;
		case '1':
			return 1;
		case '2':
			return 2;
		case '3':
			return 3;
		case '4':
			return 4;
		case '5':
			return 5;
		case '6':
			return 6;
		case '7':
			return 7;
		case '8':
			return 8;
		case '9':
			return 9;
		case 'a':
			return 10;
		case 'b':
			return 11;
		case 'c':
			return 12;
		case 'd':
			return 13;
		case 'e':
			return 14;
		case 'f':
			return 15;
		default:
			return 0;
	}
}

/**
 * FUNCTION NAME: Util_cacheWbInv
 *
 * DESCRIPTION: This function writes back and invalidates the cache. Must
 * 				be performed after writing to a shared memory location.
 *
 * ARGUMENTS:
 *				uint32_t addr: Address of data to write
 *				uint32_t size: Size of block to invalidate (in bytes)
 */
extern void Util_cacheWbInv(uint32_t addr, uint32_t size)
{
	Cache_wbInv((xdc_Ptr*)addr, size, ti_sysbios_interfaces_ICache_Type_ALL, 1);
}

/**
 * FUNCTION NAME: Util_cacheInv
 *
 * DESCRIPTION: This function invalidates the cache. Must be performed before
 * 				reading a shared memory location.
 *
 * ARGUMENTS:
 * 				uint32_t addr: Address of data to write
 * 				uint32_t size: Size of block to invalidate (in bytes)
 */
extern void Util_cacheInv(uint32_t addr, uint32_t size)
{
	Cache_inv((xdc_Ptr*)addr, size, ti_sysbios_interfaces_ICache_Type_ALL, 1);
}

/**
 * FUNCTION NAME: Util_WriteAddr
 *
 * DESCRIPTION: This function writes a global address ensuring cache operations
 * 				occur
 *
 * ARGUMENTS:
 * 				uint32_t addr:  Address of data to write
 * 				uint32_t value: Value to write to address
 */
extern void Util_writeAddr(uint32_t addr, uint32_t value)
{
	*(uint32_t*)addr = value;
	Util_cacheWbInv(addr, BYTES_WORD);
}

/**
 * FUNCTION NAME: Util_ReadAddr
 *
 * DESCRIPTION: This function reads a global address ensuring cache operations
 * 				occur
 * ARGUMENTS:
 * 				uint32_t addr:   Address of data to read
 * 				uint32_t *value: Pointer to data read
 */
extern void Util_readAddr(uint32_t addr, uint32_t *value)
{
	Util_cacheInv(addr, BYTES_WORD);
	*value = *(uint32_t*)addr;
}

/**
 * FUNCTION NAME: Util_ClearCtiInt
 *
 * DESCRIPTION: This function clears DebugSS CTI EMU1 trigger
 */
extern void Util_clearCtiInt(void)
{
	//Re-arm AET RTOS interrupt and EMU1 trigger
	AET_C66x_GDB_Intr_Rearm();
}

/**
 * FUNCTION NAME: Util_initArmDspIpc
 *
 * DESCRIPTION: This function initializes ARM-C66xDSP IPC for GDB
 *
 * RETURN:		Error code
 *
 */
extern int Util_initArmDspIpc(void)
{
#ifdef DRA7xx_PLATFORM

	uint32_t reg_value;
	int32_t timeout;

	//Power up DebugSS, if not already powered up
	Util_readAddr((EMU_CM_BASE_ADDR+CM_EMU_CLKSTCTRL), &reg_value);

	if((reg_value & 0x100) != 0x100)
	{
		Util_writeAddr((EMU_CM_BASE_ADDR+CM_EMU_CLKSTCTRL), ((reg_value & 0xFFFFFFFC) | 0x2));

		for(reg_value=0;reg_value<0xFFFF;reg_value++);

		timeout = 100000;

		while(timeout > 0)
		{
			/* Read CM_EMU_CLKSTCTRL */
			Util_readAddr((EMU_CM_BASE_ADDR+CM_EMU_CLKSTCTRL), &reg_value);

			if((reg_value & 0x100) == 0x100)
			{
				break;
			} else {
				timeout--;
			}
		}

		if(timeout <= 0)
		{
			return ERR_ARM_DSP_IPC_INIT;
		}

	}

	//Enable L3 instrumentation clocks, if not already enabled

	/* Read CM_L3INSTR_L3_3_CLKCTRL */
        Util_readAddr((CM_L3INSTR_L3_3_BASE_ADDR+CM_L3INSTR_L3_3_CLKCTRL), &reg_value);

	if(0x1 != reg_value)
	{
		Util_writeAddr((CM_L3INSTR_L3_3_BASE_ADDR+CM_L3INSTR_L3_3_CLKCTRL), 0x1);
	}

	/* Read CM_L3INSTR_L3_INSTR_CLKCTRL */
	Util_readAddr((CM_L3INSTR_L3_3_BASE_ADDR+CM_L3INSTR_L3_INSTR_CLKCTRL), &reg_value);

	if(0x1 != reg_value)
	{
		Util_writeAddr((CM_L3INSTR_L3_3_BASE_ADDR+CM_L3INSTR_L3_INSTR_CLKCTRL), 0x1);
	}

	//Check if DebugSS PWR/CLK is configured, if not configure the same

	/* Read PLLCTRL_STATUS */
	Util_readAddr((DPLL_DEBUG_BASE_ADDR+PLLCTRL_STATUS_REG), &reg_value);

	/* check if DebugSS DPLL is locked */
	if(0 == (reg_value & 0x2))
	{
		// PLL CONFIGURATION1
		// M5 [30:26] = 0x9; M4 [25:21] = 0x4;
		// M  [20:9] = 0x1DB (475); N  [8:1] = 0x9
		Util_writeAddr((DPLL_DEBUG_BASE_ADDR+PLLCTRL_CONFIG1_REG), 0x2483B612);

		// PLL CONFIGURATION2
		Util_writeAddr((DPLL_DEBUG_BASE_ADDR+PLLCTRL_CONFIG2_REG), 0xE52008);

		// Write 0x1 to PLLCTRL_GO
		Util_writeAddr((DPLL_DEBUG_BASE_ADDR+PLLCTRL_GO_REG), 0x1);

		//check if PLL locked or timeout after waiting for a predefined period
                timeout = 100000;
		while(timeout > 0)
		{
			/* Read PLLCTRL_STATUS */
			Util_readAddr((DPLL_DEBUG_BASE_ADDR+PLLCTRL_STATUS_REG), &reg_value);

			if(0x2 == (reg_value & 0x2))
			{
				break;
			} else {
				timeout--;
			}
		}

		if(timeout <= 0)
		{
			return ERR_ARM_DSP_IPC_INIT;
		}	

	}

#endif

#ifdef KEYSTONE_PLATFORM

	uint32_t reg_value;
	int32_t timeout;

	//Power up DebugSS, if not already powered up
	Util_readAddr(PSC_DEBUGSS_MDSTAT, &reg_value);

	if((reg_value & 0x1F) != PSC_ENABLE)
	{
		
		//wait PSC_GOSTAT to clear
		timeout = 100000;

		while(timeout > 0)
		{
			Util_readAddr(PSC_PTSTAT, &reg_value);

			if((reg_value & (0x1 << PSC_DEBUGSS_PD)) == 0x0)
			{
				break;
			} else {
				timeout--;
			}
		}

		if(timeout <= 0)
		{
			return ERR_ARM_DSP_IPC_INIT;
		}

		// Set power domain control
		Util_readAddr(PSC_DEBUGSS_PDCTL, &reg_value);
		reg_value = reg_value | 0x00000001;
                Util_writeAddr(PSC_DEBUGSS_PDCTL, reg_value);

		// Set MDCTL NEXT to new state
		Util_readAddr(PSC_DEBUGSS_MDCTL, &reg_value);
		reg_value = (reg_value & ~(0x1f)) | PSC_ENABLE;
                Util_writeAddr(PSC_DEBUGSS_MDCTL, reg_value);

		// Start power transition by setting PTCMD GO to 1
		Util_readAddr(PSC_PTCMD, &reg_value);
		reg_value = reg_value | (0x1<<PSC_DEBUGSS_PD);
                Util_writeAddr(PSC_PTCMD, reg_value);

		//wait PSC_GOSTAT to clear
		timeout = 100000;

		while(timeout > 0)
		{
			Util_readAddr(PSC_PTSTAT, &reg_value);

			if((reg_value & (0x1 << PSC_DEBUGSS_PD)) == 0x0)
			{
				break;
			} else {
				timeout--;
			}
		}

		if(timeout <= 0)
		{
			return ERR_ARM_DSP_IPC_INIT;
		}

		//Verify if DebugSS is powered up
		Util_readAddr(PSC_DEBUGSS_MDSTAT, &reg_value);

		if((reg_value & 0x1F) != PSC_ENABLE)
		{
			return ERR_ARM_DSP_IPC_INIT;
	        }
		
	}

#endif

	//clear DebugSS CTI EMU1 trigger
	Util_clearCtiInt();

	// setup AET HW to generate AET RTOS INT on EMU1 trigger
	return (AET_C66x_GDB_Setup());
}





