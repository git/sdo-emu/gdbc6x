/*
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Monitor.h"


#define MACRO_IS_64PLUS_COMPACT_HEADER(Opcode)   ((Opcode & 0xF0000000) == 0xE0000000)
#define MACRO_IS_64PLUS_COMPACT_SLOT(Opcode,Idx) (((((Opcode >> 21) & 0x7F) >> Idx) & 0x01) == 0x01)
#define MACRO_IS_C6X_NOP(Opcode)                 ((Opcode & 0xF0021FFE) == 0x00000000)
#define MACRO_C6X_NOP_EXTRACT(Opcode)            (((Opcode >> 13) & 0xF) + 1)
#define MACRO_IS_C64_BNOP(Opcode)                ((Opcode & 0x00001FFC) == 0x00000120)
#define MACRO_IS_C64P_CALLP(Opcode)              ((Opcode & 0xF000007C) == 0x10000010)
#define MACRO_IS_C64_ADDKPC(Opcode)              ((Opcode & 0x00001FFC) == 0x00000160)
#define MACRO_BRANCH_NOP_EXTRACT(Opcode)         ((Opcode >> 13) & 0x7)
// FPH -- check to see if protected-load bit is set
#define MACRO_IS_C64P_COMPACT_HEADER_PL(Opcode)  (((Opcode >> 20) & 0x01) == 0x01)
/* check to see if instruction is LL type load. */
#define MACRO_IS_LL_LD(Opcode)  ((Opcode & 0x0003E07C) == 0x00000040)
/* check to see if instruction is LDDW type load. */
#define MACRO_IS_LDDW_LD(Opcode) ((Opcode & 0x0001400C) == 0x00004008)
/* check to see if instruction is LDNDW and LDNW and LDB/LDH/LDW 5-bit offset type load. */
#define MACRO_IS_LDN_LD(Opcode)  (((Opcode & 0x0000000C) == 0x00000004) && ((Opcode & 0x00014000) != 0x00004000))
/* check to see if instruction is LDB/LDH/LDW 15-bit offset type load */
#define MACRO_IS_LD15_LD(Opcode) (((Opcode & 0x0000000C) == 0x0000000C) && ((Opcode & 0x00000030) != 0x00000030))
#define PBIT            0x00000001				/* Parallel execute opcode bit      */
/* Unop -- check to see if instruction is an unconditional mcycle-NOP */
#define MACRO_IS_COMPACT_NOP(Opcode)        ((Opcode & 0x1FFF) == 0x0C6E)
/* Unop -- extract the number of cycles for an unconditional mcycle-NOP */
#define MACRO_COMPACT_NOP_EXTRACT(Opcode)   (((Opcode >> 13) & 0x7) + 1)
/* Sbs7, Sbu8 -- check to see if instruction is an unconditional displacement BNOP */
#define MACRO_IS_COMPACT_UNC_DISP_BNOP(Opcode) (((Opcode) & 0x3E) == 0x0A)
/* Sbs7c, Sbu8c -- check to see if instruction is a conditional displacement BNOP */
#define MACRO_IS_COMPACT_CON_DISP_BNOP(Opcode) (((Opcode) & 0x2E) == 0x2A)
/* Sx1b -- check to see if instruction is an unconditional register BNOP */
#define MACRO_IS_COMPACT_UNC_REG_BNOP(Opcode)  (((Opcode) & 0x187F) == 0x006F)
/* Sbs7(c), Sbu8(c), Sx1b -- extract the # of cycles for a BNOP */
#define MACRO_COMPACT_BNOP_NOP_EXTRACT(Opcode) ((0xC000 == (Opcode & 0xC000)) ? 5 : ((Opcode >> 13) & 0x7))
/* Scs10 -- check to see if instruction is an unconditional CALLP */
#define MACRO_IS_COMPACT_CALLP(Opcode)  ((Opcode & 0x3E) == 0x1A)
/* FPH -- check to see if parallel-bit is set for a specified IP */
#define MACRO_IS_C64P_COMPACT_PBIT(Opcode, FPidx, IPidx) (((Opcode >> (FPidx*2 + IPidx)) & 0x01) == 0x01)
/* Doff4,Doff4DW -- check to see if instruction is a LD */
#define MACRO_IS_COMPACT_OFF_LD(Opcode) ((Opcode & 0x040E) == 0x000C)
/* Dind,DindDW -- check to see if instruction is a LD */
#define MACRO_IS_COMPACT_IND_LD(Opcode) ((Opcode & 0x0C0E) == 0x040C)
/* Dinc,DincDW -- check to see if instruction is a LD */
#define MACRO_IS_COMPACT_INC_LD(Opcode) ((Opcode & 0xCC0E) == 0x0C0C)
/* Ddec,DdecDW -- check to see if instruction is a LD */
#define MACRO_IS_COMPACT_DEC_LD(Opcode) ((Opcode & 0xCC0E) == 0x4C0C)
/* Dstk -- check to see if instruction is a LD */
#define MACRO_IS_COMPACT_STK_LD(Opcode) ((Opcode & 0x8C0E) == 0x8C0C)
/* Dpp -- check to see if instruction is a LD */
#define MACRO_IS_COMPACT_PP_LD(Opcode)  ((Opcode  & 0x487F) == 0x4077)

#define SWBP_32OPCODE_ZP_MASK   0x00000003
#define MACRO_SWBP_32OPCODE(id_field) (SWBP_32BIT | ((id_field & 0x1F) << 18))
#define MACRO_SWBP_16OPCODE(id_field) (SWBP_16BIT | (( (id_field & 0x18) << 11) | ((id_field & 0x07) << 7 )) )

#define MACRO_COMPACT_OPCODE_REPLACE(upper_hw, wa_memory, replace)	\
        ( upper_hw ? ((wa_memory & 0xFFFF) | ((replace & 0xFFFF) << 16)) : \
                     ((wa_memory & (uint32_t)(0xFFFF << 16)) | (replace & 0xFFFF)) )

/**
 * Finds the necessary attributes for setting a breakpoint
 */
static void bpAttributes(uint32_t *breakAddr, uint32_t *instruction, int *breakLength, uint32_t *breakIns, bool useUnsafeBp);

/**
 * Function for adjusting the breakpoint address to an interruptible
 * boundary
 */
static uint32_t adjustAddress(bool useUnsafeBp, uint32_t breakAddr);

/**
 * Function for getting the length of an instruction
 */
static int getInstructionLength(uint32_t address);

/**
 * FUNCTION NAME: setSWBP
 *
 * DESCRIPTION: Function for setting software breakpoints.
 *
 * ARGUMENTS:
 * 				Mon_Session *monSession: Pointer to the Monitor session
 * 				uint32_t      breakAddr: The address to set the breakpoint
 *
 * RETURN:		Error code
 *
 */
extern int setSWBP(struct Mon_Session *monSession, uint32_t breakAddr, int breakLength)
{
	uint32_t instruction   = 0;
	uint32_t breakIns      = 0;
	int error              = NO_ERR;
	int i                  = 0;

	if(NO_ERR == error)
	{
		// get the breakpoint attributes
		bpAttributes(&breakAddr, &instruction, &breakLength, &breakIns, monSession->bpHandler.useUnsafeBp);

		// ensure GDB sends an acceptable address
		if(BP_ATTRIBUTE_16BIT == breakLength)
		{
			// for 16 bit instructions, LSB must not be set
			if(breakAddr & 0x1)
			{
				error = ERR_NO_BP;
			}
          
			// ensure breakpoint is not already present
			if((instruction & BP16_MASK) == SWBP_16BIT)
			{
				error = ERR_NO_BP;
			}
		}

		// ensure GDB sends an acceptable address
		if(BP_ATTRIBUTE_32BIT == breakLength)
		{
			// for 32 bit instructions, 2 LSBs must not be set
			if(breakAddr & 0x3)
			{
				error = ERR_NO_BP;
			}

			// ensure breakpoint is not already present
			if((instruction & BP32_MASK) == SWBP_32BIT)
			{
				error = ERR_NO_BP;
			}
		}
          

		// save the necessary parameters
		if(NO_ERR == error)
		{
			if(monSession->bpHandler.numBreakpoints < NUM_BREAKPOINTS)
			{
				// populate first open spot in breakpoint data structure
				for(i = 0; i < NUM_BREAKPOINTS; i++)
				{
					if(0 == monSession->bpHandler.bpAttributes[i])
					{
						// set the breakpoint attributes and save the opcodes
						if(BP_ATTRIBUTE_16BIT == breakLength)
						{
							monSession->bpHandler.bpAttributes[i] = BP_ATTRIBUTE_16BIT | BP_ATTRIBUTE_SET;
						}
						else
						{
							monSession->bpHandler.bpAttributes[i] = BP_ATTRIBUTE_32BIT | BP_ATTRIBUTE_SET;
						}
						monSession->bpHandler.bpInstructions[i] = instruction;
						monSession->bpHandler.bpAddress[i] = breakAddr;
						monSession->bpHandler.numBreakpoints++;
						break;
					}
				}

				// set the breakpoint
				memcpy((void *)breakAddr, &breakIns, breakLength);

				// write the opcode through the cache
				Util_cacheWbInv(breakAddr, breakLength);
			}
			else
			{
				error = ERR_NO_BP;
			}
		}
		else
		{
			error = ERR_NO_BP;
		}
	}

	return error;
}

/**
 * FUNCTION NAME: bpAttributes
 *
 * DESCRIPTION: Finds the necessary attributes for setting a breakpoint and
 * 	            generates the software breakpoint opcode.
 *
 * ARGUMENTS:
 * 				uint32_t    breakAddr: The address to set the breakpoint
 * 				uint32_t *instruction: Pointer to the field to be populated with the
 * 				                       instruction being replaced
 * 				int      *breakLength: Pointer to the field to be populated with the
 * 					                   length of the breakpoint opcode. Either
 * 				                       BP_ATTRIBTUTE_32BIT (32-bit) or BP_ATTRIBUTE_16BIT (16-bit)
 * 				uint32_t    *breakIns: Pointer to the field to be populated with the
 * 					                   software breakpoint opcode instruction.
 */
static void bpAttributes(uint32_t *breakAddr, uint32_t *instruction, int *breakLength, uint32_t *breakIns, bool useUnsafeBp)
{
	uint32_t fPacketHeader  = 0;
	uint32_t fPacketAddr    = 0;
	int instructionDuration = 1;

	///////////////////////////////////////////////////////////////////////////////
	// pulled from the C64x+ emulation driver
	///////////////////////////////////////////////////////////////////////////////

	// adjust the address
	*breakAddr = adjustAddress(useUnsafeBp, *breakAddr);

	fPacketAddr = ((*breakAddr & ~0x1F) | 0x1C);
	Util_readAddr(fPacketAddr, &fPacketHeader);

	///////////////////////////////////////////////////////////////////////////////
	// The instruction size can be determined to be 4-bytes or 2-bytes using the
	// following steps:
	// 1) If instruction is at FPH Address then it is necessarily 4-bytes
	// 2) If not in a headered fetch packet then it is necessarily 4-bytes
	// 3) If in a headered fetch packet at a compact location then it is 2-bytes
	// 4) If in a headered fetch packet at a word location then it is 4-bytes
	///////////////////////////////////////////////////////////////////////////////

	// only find length if it was not provided or if this is a safe breakpoint
	if((*breakLength != BP_ATTRIBUTE_32BIT) && (*breakLength != BP_ATTRIBUTE_16BIT) &&
	   (!useUnsafeBp))
	{
		*breakLength = getInstructionLength(*breakAddr);
	}

	// read the instruction to be replaced
	Util_cacheInv(*breakAddr, *breakLength);
	memcpy(instruction, (void *)(*breakAddr), *breakLength);

	///////////////////////////////////////////////////////////////////////////////
	// We now must find the duration of the opcode to be replaced in order to
	// use the correct SWBP instruction.
	// The instruction duration can be determined by examining the opcode.
	//
	// Most instruction packets are 1-cycle in length; however, there are some
	// exceptions to this.  Mainly:
	//
	// 1) Fetch Packet Header(.fphead) -- 0 cycles
	// 2) ADDKPC<n>,BNOP<n>,NOP<n> -- n + 1 cycles (0 <= n <= 5)
	// 3) CALLP -- 6 cycles
	// 4) Protected Load (LD)  -- 5 cycles
	////////////////////////////////////////////////////////////////////////////////

	// check for the FPH case
	if(((*breakAddr & ~0x3) == fPacketAddr) &&
			MACRO_IS_64PLUS_COMPACT_HEADER(fPacketHeader))
	{
		instructionDuration = 0;
	}
	else if(BP_ATTRIBUTE_32BIT == *breakLength)
	{
		// check for multi-cycle NOP instruction
		if(MACRO_IS_C6X_NOP(*instruction))
		{
			instructionDuration = MACRO_C6X_NOP_EXTRACT(*instruction);
		}

		// check for multi-cycle branch instruction
		else if(MACRO_IS_C64_BNOP(*instruction) ||
		        MACRO_IS_C64_ADDKPC(*instruction) ||
		        MACRO_IS_C64P_CALLP(*instruction))
		{
			instructionDuration = MACRO_BRANCH_NOP_EXTRACT(*instruction);
		}

		// check for protected load setting in fetch packet header
		else if(MACRO_IS_64PLUS_COMPACT_HEADER(fPacketHeader) &&
		        MACRO_IS_C64P_COMPACT_HEADER_PL(fPacketHeader))
		{
			// check for load instruction
			if( MACRO_IS_LL_LD(*instruction) ||
			    MACRO_IS_LDDW_LD(*instruction) ||
			    MACRO_IS_LDN_LD(*instruction) ||
			    MACRO_IS_LD15_LD(*instruction) )
			{
				instructionDuration = 5;
			}
	    }
	}
	// find the 16-bit instruction duration
	else if(BP_ATTRIBUTE_16BIT == *breakLength)
	{
		// check for multi-cycle NOP instruction
		if(MACRO_IS_COMPACT_NOP(*instruction))
		{
			instructionDuration = MACRO_COMPACT_NOP_EXTRACT(*instruction);
		}

		// check for multi-cycle branch instruction
		else if(MACRO_IS_COMPACT_UNC_DISP_BNOP(*instruction) ||
		        MACRO_IS_COMPACT_CON_DISP_BNOP(*instruction) ||
		        MACRO_IS_COMPACT_UNC_REG_BNOP(*instruction))
		{
			instructionDuration = MACRO_COMPACT_BNOP_NOP_EXTRACT(*instruction);
		}

		// check for CALLP instrution (implied 5 cycles)
		else if(MACRO_IS_COMPACT_CALLP(*instruction))
		{
			instructionDuration = 5;
		}

		// check for protected load setting in fetch packet header
		else if(MACRO_IS_C64P_COMPACT_HEADER_PL(fPacketHeader))
		{
			// check for load instruction
			if(MACRO_IS_COMPACT_OFF_LD(*instruction) ||
			   MACRO_IS_COMPACT_IND_LD(*instruction) ||
			   MACRO_IS_COMPACT_INC_LD(*instruction) ||
			   MACRO_IS_COMPACT_DEC_LD(*instruction) ||
			   MACRO_IS_COMPACT_STK_LD(*instruction) ||
			   MACRO_IS_COMPACT_PP_LD(*instruction))
			{
				instructionDuration = 4;
			}
		}
	}

	// set the breakpoint opcodes
	if (BP_ATTRIBUTE_16BIT == *breakLength)
	{
		*breakIns = MACRO_SWBP_16OPCODE(instructionDuration - 1);
	}
	else if(BP_ATTRIBUTE_32BIT == *breakLength)
	{
		*breakIns = (*instruction & SWBP_32OPCODE_ZP_MASK) | MACRO_SWBP_32OPCODE(instructionDuration - 1);
	}
}

/**
 * FUNCTION NAME: removeSWBP
 *
 * DESCRIPTION: Function for removing software breakpoints.
 *
 * ARGUMENTS:
 * 				Mon_Session *monSession: Pointer to the Monitor session
 * 				uint32_t      breakAddr: The address to clear the breakpoint at
 */
extern void removeSWBP(struct Mon_Session *monSession, uint32_t breakAddr)
{
	int i = 0;

	for(i = 0; i < NUM_BREAKPOINTS; i++)
	{
		if(breakAddr == monSession->bpHandler.bpAddress[i])
		{
			// reset the opcode
			if(monSession->bpHandler.bpAttributes[i] & BP_ATTRIBUTE_SET)
			{
				if(monSession->bpHandler.bpAttributes[i] & BP_ATTRIBUTE_16BIT)
				{
					memcpy((void *)breakAddr, &monSession->bpHandler.bpInstructions[i], BP_ATTRIBUTE_16BIT);
					Util_cacheWbInv(breakAddr, BP_ATTRIBUTE_16BIT);
				}
				else
				{
					memcpy((void *)breakAddr, &monSession->bpHandler.bpInstructions[i], BP_ATTRIBUTE_32BIT);
					Util_cacheWbInv(breakAddr, BP_ATTRIBUTE_32BIT);
				}

				// clear the breakpoint by clearing the saved data
				monSession->bpHandler.bpAttributes[i] = 0;
				monSession->bpHandler.bpAddress[i] = 0;
				monSession->bpHandler.bpInstructions[i] = 0;
				monSession->bpHandler.numBreakpoints--;
				break;
			}
		}
	}
}

/**
 * FUNCTION NAME: clearAllSWBP
 *
 * DESCRIPTION: Function for removing ALL software breakpoints.
 *
 * ARGUMENTS:
 * 				Mon_Session *monSession: Pointer to the Monitor session
 */
extern void clearAllSWBP(struct Mon_Session *monSession)
{
	int i = 0;
	for(i = 0; i < NUM_BREAKPOINTS; i++)
	{
		if(monSession->bpHandler.bpAttributes[i] & BP_ATTRIBUTE_SET)
		{
			// reset the opcode
			if(BP_ATTRIBUTE_16BIT & monSession->bpHandler.bpAttributes[i])
			{
				memcpy((void *)monSession->bpHandler.bpAddress[i], &monSession->bpHandler.bpInstructions[i], BP_ATTRIBUTE_16BIT);
				Util_cacheWbInv(monSession->bpHandler.bpAddress[i], BP_ATTRIBUTE_16BIT);
			}
			else
			{
				memcpy((void *)monSession->bpHandler.bpAddress[i], &monSession->bpHandler.bpInstructions[i], BP_ATTRIBUTE_32BIT);
				Util_cacheWbInv(monSession->bpHandler.bpAddress[i], BP_ATTRIBUTE_32BIT);
			}

			// clear the breakpoint by clearing the saved data
			monSession->bpHandler.bpAttributes[i] = 0;
			monSession->bpHandler.bpAddress[i] = 0;
			monSession->bpHandler.bpInstructions[i] = 0;
			monSession->bpHandler.numBreakpoints--;
		}
	}
}

/**
 * FUNCTION NAME: adjustAddress
 *
 * DESCRIPTION: Function for adjusting the breakpoint address to an interruptible
 *              boundary
 *
 * ARGUMENTS:
 * 				bool   useUnsafeBp: The type of breakpoint being used: safe or unsafe
 * 				uint32_t breakAddr: The address being requested to set a breakpoint on
 *
 * RETURN:      An adjusted breakpoint address
 */
static uint32_t adjustAddress(bool useUnsafeBp, uint32_t breakAddr)
{
	return breakAddr;
}

/**
 * FUNCTION NAME: getInstructionLength
 *
 * DESCRIPTION: Function for getting the length of an instruction
 *
 * ARGUMENTS:
 * 				uint32_t address: The address of the instruction to find
 * 				                  the length of
 *
 * RETURN:      The opcode length
 */
static int getInstructionLength(uint32_t address)
{
	int length = 0;
	uint32_t fPacketHeader = 0;
	uint32_t fPacketAddr   = ((address & ~0x1F) | 0x1C);

	Util_readAddr(fPacketAddr, &fPacketHeader);
	// check for instruction at FPH address
	// ensure to word align it first
	if((address & ~0x3) == fPacketAddr)
	{
		length = BP_ATTRIBUTE_32BIT;
	}
	else
	{
		// check for case where we are not in a headered fetch packet
		// by checking if we are in a compact header
		if(!MACRO_IS_64PLUS_COMPACT_HEADER(fPacketHeader))
		{
			length = BP_ATTRIBUTE_32BIT;
		}
		else
		{
			if(MACRO_IS_64PLUS_COMPACT_SLOT(fPacketHeader, (address & 0x1C) / 4))
			{
				length = BP_ATTRIBUTE_16BIT;
			}
			else
			{
				length = BP_ATTRIBUTE_32BIT;
			}
		}
	}

	return length;
}




